<?php

namespace AppBundle\Controller;

use FOS\UserBundle\Controller\SecurityController as BaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

class SecurityController extends BaseController
{
    // Override FOSUserBundle login action.
    public function loginAction(Request $request){

    	// Redirect me back if someone's logged in!
    	if($this->getUser()){
    		return $this->redirectToRoute('home');
    	}

    	//meaning dinadagdagan lng natin yung pinaka base or parent ng bago. which is yung nasa taas.
    	return parent::loginAction($request);

    }

     
}
	