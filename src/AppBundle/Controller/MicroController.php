<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class MicroController extends Controller
{
    /**
     * @Route("/home", name="home")
     */
    public function homePage(Request $request)
    {
        $user=$this->getUser();

        $em = $this->getDoctrine()->getManager();
        $names = $em->getRepository('AppBundle:User')->findAll();

        // // replace this example code with whatever you need
        // return $this->render('default/index.html.twig', [
        //     'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        //     
        // ]);

        return $this->render("body/body_home.html.twig", 
            ['user'=>$user,
             'names'=>$names
            ]);
    }

  
}
