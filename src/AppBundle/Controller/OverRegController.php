<?php
// src/AppBundle/Controller/ProductController.php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\User;
use AppBundle\Form\OverRegisterFormType;

class OverRegController extends Controller
{
    /**
     * @Route("/profpick", name="profpick")
     */
    public function haha(Request $request)
    {
        $user = new User();
        $form = $this->createForm(OverRegisterFormType::class, $user);
        $form->handleRequest($request);

        

        if ($form->isSubmitted() && $form->isValid()) {
            // $file stores the uploaded PDF file
            /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
            $file = $user->getProfPic();

            dump($file);
            // Generate a unique name for the file before saving it
            $fileName = md5(uniqid()).'.'.$file->guessExtension();
            dump($fileName);
            // Move the file to the directory where brochures are stored
            $file->move(
                $this->getParameter('brochures_directory'),
                $fileName
            );

            // Update the 'brochure' property to store the PDF file name
            // instead of its contents
            $user->setProfPic($fileName);
            dump($user);die();

            // ... persist the $user variable or any other work

            //return $this->redirect($this->generateUrl('app_product_list'));
        }

        return $this->render('body/body_signup.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}