<?php 
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="tbl_post")
 */
class Post 
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    public function setId($id){
        $this->id = $id;
    }
    public function getId(){
        return $this->id;
    }
    
    /**
     * 
     * @ORM\Column(type="string")
     * 
     */
    protected $username;

    public function setUsername($username){
        $this->username= $username;
    }
    public function getUsername(){
        return $this->username;
    }

    /**
     * 
     * @ORM\Column(type="string")
     * 
     */
    protected $title;

    public function setTitle($title){
        $this->title= $title;
    }
    public function getTitle(){
        return $this->title;
    }

    /**
     * 
     * @ORM\Column(type="string")
     * 
     */
    protected $topic;

    public function setTopic($topic){
        $this->topic= $topic;
    }
    public function getTopic(){
        return $this->topic;
    }

    /**
     * 
     * @ORM\Column(type="string")
     * 
     */
    protected $blog_mess;

    public function setBlogMess($blog_mess){
        $this->blog_mess= $blog_mess;
    }
    public function getBlogMess(){
        return $this->blog_mess;
    }

    /**
     * 
     * @ORM\Column(type="string")
     *  
     */
    protected $blog_img;

    public function setBlogImg($blog_img){
        $this->blog_img= $blog_img;
    }
    public function getBlogImg(){
        return $this->blog_img;
    }

    /**
     * 
     * @ORM\Column(type="string")
     * 
     */
    protected $posted_at;

    public function setPostedAt($posted_at){
        $this->posted_at= $posted_at;
    }
    public function getPostedAt(){
        return $this->posted_at;
    }

}