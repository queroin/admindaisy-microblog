<?php 
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="tbl_acctdetails")
 */
class AcctDetails 
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    public function setId($id){
        $this->id = $id;
    }
    public function getId(){
        return $this->id;
    }
    
    /**
     * 
     * @ORM\Column(type="string")
     * 
     */
    protected $username;

    public function setUsername($username){
        $this->username= $username;
    }
    public function getUsername(){
        return $this->username;
    }

    /**
     * 
     * @ORM\Column(type="string")
     * 
     */
    protected $prof_pic;

    public function setProfPic($prof_pic){
        $this->prof_pic= $prof_pic;
    }
    public function getProfPic(){
        return $this->prof_pic;
    }

    /**
     * 
     * @ORM\Column(type="string")
     * 
     */
    protected $cover_pic;

    public function setCoverPic($cover_pic){
        $this->cover_pic= $cover_pic;
    }
    public function getCoverPic(){
        return $this->cover_pic;
    }

    /**
     * 
     * @ORM\Column(type="string")
     * 
     */
    protected $about;

    public function setAbout($about){
        $this->about= $about;
    }
    public function getAbout(){
        return $this->about;
    }

}