<?php 
namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
//use Symfony\Component\Security\Core\Util\SecureRandomInterface;

/**
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * 
     * @ORM\Column(type="string")
     * 
     */
    protected $name;

    public function setName($name){
        $this->name= $name;
    }
    public function getName(){
        return $this->name;
    }

    

    /**
     * 
     * @ORM\Column(type="string")
     * 
     */
    protected $fname;

    public function setFname($fname){
        $this->fname= $fname;
    }
    public function getFname(){
        return $this->fname;
    }

    /**
     * 
     * @ORM\Column(type="string")
     * 
     */
    protected $lname;

    public function setLname($lname){
        $this->lname= $lname;
    }
    public function getLname(){
        return $this->lname;
    }


    /**
     * 
     * @ORM\Column(type="string")
     * 
     */
    protected $prof_pic;

    public function setProfPic($prof_pic){
        $this->prof_pic= $prof_pic;
    }
    public function getProfPic(){
        return $this->prof_pic;
    }


    /**
     * 
     * @ORM\Column(type="string")
     * 
     */
    protected $cover_pic;

    public function setCoverPic($cover_pic){
        $this->cover_pic= $cover_pic;
    }
    public function getCoverPic(){
        return $this->cover_pic;
    }

    /**
     * 
     * @ORM\Column(type="text", length=65535, nullable=true)
     */
    protected $about;

    public function setAbout($about){
        $this->about= $about;
    }
    public function getAbout(){
        return $this->about;
    }

    public function __construct()
    {
        parent::__construct();
        // your own logic
    }
}