<?php

/* :body:body_profile.html.twig */
class __TwigTemplate_f2dfab1a2605f39317a09a749ff38a37b4ca89816c3e8b462b5f2871bab9d629 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":body:body_profile.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "    
        <div class=\"page-header page-header-small\" filter-color=\"orange\">
            <div class=\"page-header-image\" data-parallax=\"true\" style=\"background-image: url(";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/now-ui-kit-v1.1.0/assets//img/bgg6.jpg"), "html", null, true);
        echo ");\">
            </div>
            <div class=\"container\">
                <div class=\"content-center\">
                    <div class=\"photo-container\" style=\"padding-top: 170px;\">
                        <img src=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/now-ui-kit-v1.1.0/assets//img/team_gra.jpg"), "html", null, true);
        echo "\" alt=\"Thumbnail Image\" class=\"rounded-circle img-fluid img-raised\" style=\"height: 170px; width: 170px;\">
                        <h4 class=\"title\">";
        // line 12
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "name", array()), "html", null, true);
        echo "</h4>
                        <p class=\"category\">";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "username", array()), "html", null, true);
        echo "</p>
                    </div>                    
                </div>
            </div>
        </div>
        <div class=\"section\">
            <div class=\"container\">
                ";
        // line 21
        echo "                <h3 class=\"title\">About me</h3>
                <h5 class=\"description\">";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "about", array()), "html", null, true);
        echo "</h5>
                ";
        // line 24
        echo "                
                ";
        // line 25
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_start');
        echo "
                <div class=\"row\">
                    <div class=\"col-md-12\">
                        <h3 class=\"title text-primary\"><i class=\"fa fa-edit\"></i> Blog</h3>
                    </div>
                    ";
        // line 31
        echo "                    <div class=\"col-md-12\">
                      
                        ";
        // line 33
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "title", array()), 'widget');
        echo "

                    </div>
                    ";
        // line 37
        echo "                    <div class=\"col-md-12\">
                        ";
        // line 38
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "topic", array()), 'widget');
        echo "                        
                        <datalist id=\"browsers\">  
                            <option value=\"Self improvement/Self-Hypnosis\">
                            <option value=\"Health & Fitness for Busy People\">
                            <option value=\"Language Learning Blogs\">
                            <option value=\"How to Travel on a Budget (Best hotel deals. Car rental. Trip advice.)\">
                            <option value=\"Writing Style\">
                            <option value=\"Rescued Animals\">
                            <option value=\"Ghost-hunting\">
                            <option value=\"Healthy eating blog\">
                            <option value=\"Beyond the basics of personal financial management\">
                            <option value=\"Hamburgers\">
                            <option value=\"Getting Microsoft Certified\">
                            <option value=\"Careers vs job: following your passion\">
                            <option value=\"How to have a Strong Marriage\">
                            <option value=\"Getting Microsoft Certified\">
                        </datalist>
                    </div>
                    ";
        // line 57
        echo "                    <div class=\"col-md-12\">
                        ";
        // line 58
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "blog_mess", array()), 'widget');
        echo "                        
                    </div>
                    ";
        // line 61
        echo "                   <i class=\"fa fa-paperclip\"></i> Attachment: 
                        ";
        // line 62
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "blog_img", array()), 'widget');
        echo "                        
                    <div class=\"col-sm-6\"></div>
                        <a href=\"#\" class=\"btn btn-default Default\">CLEAR</a>
                        <input type=\"submit\" class=\"btn btn-primary Primary\" value=\"POST\" />
                </div>   
                ";
        // line 67
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_end');
        echo "

    

    
        
   

    ";
        // line 80
        echo "

                ";
        // line 83
        echo "

                <div class=\"row\">
                    <div class=\"col-md-12\">
                        <h3 class=\"title\" style=\"margin-top:20px;\">Posted Blog</h3>
                    </div>
                    ";
        // line 89
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["posts"]) ? $context["posts"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
            echo " 
                    <div class=\"row\">       
                        ";
            // line 92
            echo "                        
                        ";
            // line 106
            echo "                        ";
            echo "   
                        <div class=\"col-md-12\"></div>
                        <div class=\"col-md-3 text-center\">
                            <img src=\"";
            // line 109
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/now-ui-kit-v1.1.0/assets//img/team_gra.jpg"), "html", null, true);
            echo "\" alt=\"Thumbnail Image\" class=\"rounded-circle img-raised2\" style=\"height: 120px; width: 120px; margin-top:20px;\"><br>
                            <h7 style=\"color: gray;\">";
            // line 110
            echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "username", array()), "html", null, true);
            echo "<br>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "posted_at", array()), "html", null, true);
            echo "</h7>
                        </div>
                        
                            <div class=\"col-md-9\" style=\"padding-left: 0px;\">
                                ";
            // line 115
            echo "                                <textarea disabled class=\"distxarea\" rows=\"8\" cols=\"50\" form=\"userform\" style=\"margin-top:20px;\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "title", array()), "html", null, true);
            echo "</textarea>
                            </div>
                        
                        <div class=\"col-md-3\"></div>
                            <button class=\"btn btn-simple btn-primary Primary\"><i class=\"fa fa-pencil\" style=\"font-color:#f96332; font-size:15px;\"></i></button>
                            <button class=\"btn btn-simple btn-primary Primary\"><i class=\"fa fa-trash\" style=\"font-color:#f96332; font-size:15px;\"></i></button>
                        <br>
                    </div> 
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 124
        echo "                </div>
            </div>
        </div>

        <div class=\"modal fade modal-mini modal-primary\" id=\"modal_edit\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
            <div class=\"modal-dialog\">
                <div class=\"modal-content\">
                    <div class=\"modal-header justify-content-center\">
                        <div class=\"modal-profile\">
                            <i class=\"fa fa-edit\"></i>
                        </div>
                    </div>
                    <div class=\"modal-body\">
                        <div class=\"col-md-12\">
                            <textarea name=\"editblog\" class=\"txarea\" rows=\"8\" cols=\"50\" form=\"userform\" style=\"margin-top:20px;\"></textarea>
                        </div>
                    </div>
                    <div class=\"modal-footer\">
                        
                        <button type=\"button\" class=\"btn btn-link btn-neutral\">Save</button>
                        <button type=\"button\" class=\"btn btn-link btn-neutral\" data-dismiss=\"modal\">Discard</button>
                    </div>
                </div>
            </div>
        </div>

";
    }

    public function getTemplateName()
    {
        return ":body:body_profile.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  200 => 124,  184 => 115,  175 => 110,  171 => 109,  165 => 106,  162 => 92,  155 => 89,  147 => 83,  143 => 80,  132 => 67,  124 => 62,  121 => 61,  116 => 58,  113 => 57,  92 => 38,  89 => 37,  83 => 33,  79 => 31,  71 => 25,  68 => 24,  64 => 22,  61 => 21,  51 => 13,  47 => 12,  43 => 11,  35 => 6,  31 => 4,  28 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", ":body:body_profile.html.twig", "/home/babypandalabs/microblog/app/Resources/views/body/body_profile.html.twig");
    }
}
