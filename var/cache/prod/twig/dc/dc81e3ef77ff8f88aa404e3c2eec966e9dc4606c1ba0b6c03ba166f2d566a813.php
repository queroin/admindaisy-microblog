<?php

/* base.html.twig */
class __TwigTemplate_87ed6becf910c9b2792a13650767211b5e4e49058b003e1818d159d6c04c8674 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->loadTemplate("core/header.html.twig", "base.html.twig", 1)->display($context);
        echo "    
";
        // line 2
        $this->loadTemplate("body/body_header.html.twig", "base.html.twig", 2)->display($context);
        echo "    
    ";
        // line 3
        $this->displayBlock('body', $context, $blocks);
        // line 5
        $this->loadTemplate("body/body_footer.html.twig", "base.html.twig", 5)->display($context);
        // line 6
        $this->loadTemplate("core/footer.html.twig", "base.html.twig", 6)->display($context);
        echo "   
     ";
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "    ";
    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  41 => 4,  38 => 3,  32 => 6,  30 => 5,  28 => 3,  24 => 2,  20 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "base.html.twig", "/home/babypandalabs/microblog/app/Resources/views/base.html.twig");
    }
}
