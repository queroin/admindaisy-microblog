<?php

/* :core:footer.html.twig */
class __TwigTemplate_59d3275a0650c19d1ecb27457949b67763a144f32392cfc54e318fd2c5786680 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'javascripts_lib' => array($this, 'block_javascripts_lib'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "    \t";
        $this->displayBlock('javascripts_lib', $context, $blocks);
        // line 24
        echo "       
    </body>
</html>";
    }

    // line 1
    public function block_javascripts_lib($context, array $blocks = array())
    {
        // line 2
        echo "        \t<script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/now-ui-kit-v1.1.0/assets/js/core/jquery.3.2.1.min.js\" type=\"text/javascript"), "html", null, true);
        echo "\"></script>
\t\t\t<script src=\"";
        // line 3
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/now-ui-kit-v1.1.0/assets/js/core/popper.min.js\" type=\"text/javascript"), "html", null, true);
        echo "\"></script>
\t\t\t<script src=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/now-ui-kit-v1.1.0/assets/js/core/bootstrap.min.js\" type=\"text/javascript"), "html", null, true);
        echo "\"></script>
\t\t\t<script src=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/now-ui-kit-v1.1.0/assets/js/plugins/bootstrap-switch.js"), "html", null, true);
        echo "\"></script>
\t\t\t<script src=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/now-ui-kit-v1.1.0/assets/js/plugins/nouislider.min.js\" type=\"text/javascript"), "html", null, true);
        echo "\"></script>
\t\t\t<script src=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/now-ui-kit-v1.1.0/assets/js/plugins/bootstrap-datepicker.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
\t\t\t<script src=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/now-ui-kit-v1.1.0/assets/js/plugins/bootstrap-datepicker.js\" type=\"text/javascript"), "html", null, true);
        echo "\"></script>
\t\t\t<script src=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/now-ui-kit-v1.1.0/assets/js/now-ui-kit.js\" type=\"text/javascript"), "html", null, true);
        echo "\"></script>
    \t\t<script type=\"text/javascript\">
    \t\t\t\$(document).ready(function() {
                \tnowuiKit.initSliders();
              \t});
\t\t\t\tfunction scrollToDownload() {
\t\t\t\tif (\$('.section-download').length != 0) {
            \t\t\$(\"html, body\").animate({
                \t\tscrollTop: \$('.section-download').offset().top
            \t\t}, 1000);
        \t\t}}
\t\t\t</script>

\t\t\t<script src=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/js/bootstrap.js\" type=\"text/javascript"), "html", null, true);
        echo "\"></script>
\t\t\t<script src=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/js/jquery.3.2.1.min.js\" type=\"text/javascript"), "html", null, true);
        echo "\"></script>
    \t";
    }

    public function getTemplateName()
    {
        return ":core:footer.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  81 => 23,  77 => 22,  61 => 9,  57 => 8,  53 => 7,  49 => 6,  45 => 5,  41 => 4,  37 => 3,  32 => 2,  29 => 1,  23 => 24,  20 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", ":core:footer.html.twig", "/home/babypandalabs/microblog/app/Resources/views/core/footer.html.twig");
    }
}
