<?php

/* :body:body_home.html.twig */
class __TwigTemplate_10cf1068569bf5a22a0af9c03427918317fc9e0bef914e737e523e571c75fabb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":body:body_home.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"wrapper\">
        <div class=\"page-header clear-filter\" filter-color=\"orange\">
            <div class=\"page-header-image\" data-parallax=\"true\" style=\"background-image:url(";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/now-ui-kit-v1.1.0/assets//img/bgg3.jpg"), "html", null, true);
        echo ");\"></div>
            
            <div class=\"container\">
                <div class=\"content-center brand\">
                    <img class=\"n-logo\" src=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/now-ui-kit-v1.1.0/assets/img/blog.png"), "html", null, true);
        echo "\" alt=\"\" style=\"padding-top: 80px;\">
                    <h1 class=\"h1 seo\">BLOG</h1>
                    <h3>Create. Inspire. Share. Connect.</h3>
                    <div style=\"text-align:center;\"><br><br><br>
                    <a href=\"/post/new\" class=\"btn btn-primary btn-round btn-lg btn-block\">WRITE A BLOG</a>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"section section-team text-center\">
            <div class=\"container\">
                <h2 class=\"title\">Team</h2><br>
                <div class=\"team\">
                    <div class=\"row\">
                        <div class=\"col-md-4\">
                            <div class=\"team-player\">
                                <img src=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/now-ui-kit-v1.1.0/assets//img/team_quer.jpg"), "html", null, true);
        echo "\" alt=\"Thumbnail Image\" class=\"rounded-circle img-fluid img-raised\" style=\"height: 150px; width: 150px;\">
                                <h4 class=\"title\">Quer Laconico</h4>
                                
                                
                                <p class=\"category text-primary\">Programmer</p>
                            </div>
                        </div>                       
                        <div class=\"col-md-4\">
                            <div class=\"team-player\">
                                <img src=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/now-ui-kit-v1.1.0/assets//img/team_kyle.jpg"), "html", null, true);
        echo "\" alt=\"Thumbnail Image\" class=\"rounded-circle img-fluid img-raised\" style=\"height: 150px; width: 150px;\">
                                <h4 class=\"title\">Kyle Avedana</h4>
                                <p class=\"category text-primary\">Programmer</p>
                            </div>
                        </div>
                        <div class=\"col-md-4\">
                            <div class=\"team-player\">
                                <img src=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/now-ui-kit-v1.1.0/assets//img/team_mae.jpg"), "html", null, true);
        echo "\" alt=\"Thumbnail Image\" class=\"rounded-circle img-fluid img-raised\" style=\"height: 150px; width: 150px;\">
                                <h4 class=\"title\">Mae Sibal</h4>
                                <p class=\"category text-primary\">Programmer</p>
                            </div>
                        </div>
                        <div class=\"col-md-4\">
                            <div class=\"team-player\">
                                <img src=\"";
        // line 49
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/now-ui-kit-v1.1.0/assets//img/team_gra.jpg"), "html", null, true);
        echo "\" alt=\"Thumbnail Image\" class=\"rounded-circle img-fluid img-raised\" style=\"height: 150px; width: 150px;\">
                                <h4 class=\"title\">Gra Ayun</h4>
                                <p class=\"category text-primary\">Web Designer</p>
                            </div>
                        </div>
                        <div class=\"col-md-4\">
                            <div class=\"team-player\">
                                <img src=\"";
        // line 56
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/now-ui-kit-v1.1.0/assets//img/team_era.jpg"), "html", null, true);
        echo "\" alt=\"Thumbnail Image\" class=\"rounded-circle img-fluid img-raised\" style=\"height: 150px; width: 150px;\">
                                <h4 class=\"title\">Era Vergara</h4>
                                <p class=\"category text-primary\">Web Designer</p>
                            </div>
                        </div>
                        <div class=\"col-md-4\">
                            <div class=\"team-player\">
                                <img src=\"";
        // line 63
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/now-ui-kit-v1.1.0/assets//img/team_trags.jpg"), "html", null, true);
        echo "\" alt=\"Thumbnail Image\" class=\"rounded-circle img-fluid img-raised\" style=\"height: 150px; width: 150px;\">
                                <h4 class=\"title\">Lei Tragura</h4>
                                <p class=\"category text-primary\">Web Designer</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
  

";
    }

    public function getTemplateName()
    {
        return ":body:body_home.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  113 => 63,  103 => 56,  93 => 49,  83 => 42,  73 => 35,  61 => 26,  42 => 10,  35 => 6,  31 => 4,  28 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", ":body:body_home.html.twig", "/home/babypandalabs/microblog/app/Resources/views/body/body_home.html.twig");
    }
}
