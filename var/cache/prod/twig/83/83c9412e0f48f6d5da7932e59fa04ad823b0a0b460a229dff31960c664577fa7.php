<?php

/* :body:body_signup.html.twig */
class __TwigTemplate_6db5ebc964fbc1401b3367c797e8eef956b7be6dd70461d3743f5b75d2700fc5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":body:body_signup.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        // line 3
        echo "
<body>
";
        // line 6
        echo "
 
";
        // line 9
        echo "    <div class=\"page-header\" filter-color=\"orange\">
        <div class=\"page-header-image\" style=\"background-image:url(";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/now-ui-kit-v1.1.0/assets/img/login.jpg"), "html", null, true);
        echo ")\"></div>
        <div class=\"container\">
            <div class=\"col-md-4 content-center\">
                <div class=\"card card-signup\" data-background-color=\"orange\">
                   \t<div class=\"header text-center\">
                        <h4 class=\"title title-up\">Sign Up</h4>
                   \t</div>
                    <div class=\"card-body\">
                    \t";
        // line 19
        echo "                        <div class=\"input-group form-group-no-border\">
                            <span class=\"input-group-addon\"><i class=\"fa fa-user\"></i></span>
                         \t<input name=\"fname\" class=\"form-control\" placeholder=\"First Name\" type=\"text\">
                        </div>
                        ";
        // line 24
        echo "                        <div class=\"input-group form-group-no-border\">
                            <span class=\"input-group-addon\"><i class=\"fa fa-user\"></i></span>
                         \t<input name=\"lname\" class=\"form-control\" placeholder=\"Last Name\" type=\"text\">
                        </div>
                        ";
        // line 29
        echo "                        <div class=\"input-group form-group-no-border\">
                            <span class=\"input-group-addon\"><i class=\"fa fa-user\"></i></span>
                         \t<input name=\"username\" class=\"form-control\" placeholder=\"Username\" type=\"text\">
                        </div>
                        ";
        // line 34
        echo "                        <div class=\"input-group form-group-no-border\">
                            <span class=\"input-group-addon\"><i class=\"fa fa-at\"></i></span>
                         \t<input name=\"email\" class=\"form-control\" placeholder=\"Email\" type=\"text\">
                        </div>
                        ";
        // line 39
        echo "                        <div class=\"input-group form-group-no-border\">
                            <span class=\"input-group-addon\"><i class=\"fa fa-lock\"></i></span>
                         \t<input name=\"password\" class=\"form-control\" placeholder=\"Password\" type=\"text\">
                        </div>

                        <div class=\"footer text-center\">
                            <a href=\"/lucky/profile\" class=\"btn btn-neutral btn-round btn-lg\">GET STARTED</a>
                        </div>                            
                \t</div>
            \t</div>
        \t</div>

";
        // line 52
        echo "        \t<footer class=\"footer\">
            \t<div class=\"container\">
                \t<div class=\"copyright\">&copy;
                    \t<script>document.write(new Date().getFullYear())</script>, Designed by
                    \t<a href=\"#\" target=\"_blank\">After Laughter</a>. Coded by 
                    \t<a href=\"#\" target=\"_blank\">After Laughter</a>.
                \t</div>
           \t\t</div>
        \t</footer>    
    \t</div>
    </div>

";
    }

    public function getTemplateName()
    {
        return ":body:body_signup.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 52,  77 => 39,  71 => 34,  65 => 29,  59 => 24,  53 => 19,  42 => 10,  39 => 9,  35 => 6,  31 => 3,  28 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", ":body:body_signup.html.twig", "/home/babypandalabs/microblog/app/Resources/views/body/body_signup.html.twig");
    }
}
