<?php

/* :body:body_settings.html.twig */
class __TwigTemplate_1e0b41af3a02b7ce7a73c0e2dd0e0d1bc7e141691592764697b94aa62d1d58f3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":body:body_settings.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        // line 3
        echo "
";
        // line 5
        echo "<body class=\"index-page sidebar-collapse\">
    <nav class=\"navbar navbar-expand-lg bg-primary fixed-top\" color-on-scroll=\"400\">
        <div class=\"container\">
            <div class=\"collapse navbar-collapse justify-content-end\" id=\"navigation\" data-nav-image=\"./assets/img/blurred-image-1.jpg\">
                <ul class=\"navbar-nav\">
                    <li class=\"nav-item\">
                        <a class=\"nav-link\" href=\"/lucky/profile\">
                            <i class=\"fa fa-heart\"></i>
                            <p><strong style=\"font-size: 13px;\">Hi, First Name!</strong></p>
                        </a>
                    </li>
                    <li class=\"nav-item\">
                        <a class=\"nav-link\" href=\"/lucky/home\">
                            <i class=\"fa fa-home\"></i>
                            <p p class=\"d-lg-none d-xl-none\">Home</p>
                        </a>
                    </li>
                    <li class=\"active nav-item\">
                        <a class=\"nav-link\" href=\"/lucky/settings\">
                            <i class=\"fa fa-gear\"></i>
                            <p class=\"d-lg-none d-xl-none\">Settings</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

";
        // line 34
        echo "    <div class=\"wrapper\">
        <div class=\"main\">
            <div class=\"section section-tabs\">
                
            </div>
        </div>


";
        // line 43
        echo "        <footer class=\"footer\" data-background-color=\"black\">
            <div class=\"container\">
                <div class=\"copyright\">&copy;
                    <script>document.write(new Date().getFullYear())</script>, Designed by
                    <a href=\"#\" target=\"_blank\">After Laughter</a>. Coded by 
                    <a href=\"#\" target=\"_blank\">After Laughter</a>.
                </div>
            </div>
        </footer>    

";
    }

    public function getTemplateName()
    {
        return ":body:body_settings.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  74 => 43,  64 => 34,  34 => 5,  31 => 3,  28 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", ":body:body_settings.html.twig", "/home/babypandalabs/microblog/app/Resources/views/body/body_settings.html.twig");
    }
}
