<?php

/* :body:body_header.html.twig */
class __TwigTemplate_5292fe9600fad48e5816ccb2ddb1114bc97113f0c338fabe0dc810472177b96b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "<body class=\"index-page sidebar-collapse\">
    <nav class=\"navbar navbar-expand-lg bg-primary fixed-top\" color-on-scroll=\"400\">
        <div class=\"container\">
            <div class=\"collapse navbar-collapse justify-content-end\" id=\"navigation\" data-nav-image=\"./assets/img/blurred-image-1.jpg\">
                <ul class=\"navbar-nav\">
                    <li class=\"nav-item\">
                        <a class=\"nav-link\" href=\"/post/new\">
                            <i class=\"fa fa-heart\"></i>
                            ";
        // line 10
        if ($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array())) {
            // line 11
            echo "                            <p><strong style=\"font-size: 13px;\">Hi, ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "username", array()), "html", null, true);
            echo "</strong></p>
                            ";
        }
        // line 13
        echo "                        </a>
                    </li>
                    <li class=\"active nav-item\">
                        <a class=\"nav-link\" href=\"/home\">
                            <i class=\"fa fa-home\"></i>
                            <p p class=\"d-lg-none d-xl-none\">Home</p>
                        </a>
                    </li>
                    <li class=\"nav-item\">
                        <a class=\"nav-link\" href=\"";
        // line 22
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_profile_edit");
        echo "\">
                            <i class=\"fa fa-gear\"></i>
                            <p class=\"d-lg-none d-xl-none\">Settings</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>";
    }

    public function getTemplateName()
    {
        return ":body:body_header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  48 => 22,  37 => 13,  31 => 11,  29 => 10,  19 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", ":body:body_header.html.twig", "/home/babypandalabs/microblog/app/Resources/views/body/body_header.html.twig");
    }
}
