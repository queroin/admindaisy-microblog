<?php

/* :body:body_signin.html.twig */
class __TwigTemplate_f35d9be1fc607523643547de87f3f8e36bc862dbd41e349739e121a2677b7576 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":body:body_signin.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        // line 3
        echo "
<body class=\"login-page\">
";
        // line 6
        echo "
";
        // line 8
        echo "    <div class=\"page-header\" filter-color=\"orange\">
        <div class=\"page-header-image\" style=\"background-image:url(";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/now-ui-kit-v1.1.0/assets/img/login.jpg"), "html", null, true);
        echo ")\"></div>
        <div class=\"container\">
            <div class=\"col-md-4 content-center\">
                <div class=\"card card-login card-plain\">
                    <form class=\"form\" method=\"\" action=\"\">
                        <div class=\"header header-primary text-center\">
                            <div class=\"logo-container\">
                                <img src=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/now-ui-kit-v1.1.0/assets/img/blog.png"), "html", null, true);
        echo "\" alt=\"\">
                            </div>
                        </div>
                        <div class=\"content\">
                            ";
        // line 21
        echo "                            <div class=\"input-group form-group-no-border input-lg\">
                                <span class=\"input-group-addon\"><i class=\"fa fa-user\"></i></span>
                                ";
        // line 23
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "Email", array()), 'widget');
        echo "
                            </div>
                            ";
        // line 26
        echo "                            <div class=\"input-group form-group-no-border input-lg\">
                                <span class=\"input-group-addon\"><i class=\"fa fa-lock\"></i></span>
                                ";
        // line 28
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "Password", array()), 'widget');
        echo "
                            </div>
                        </div>
                        <div class=\"footer text-center\">
                            ";
        // line 32
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "Login", array()), 'widget');
        echo "
                        </div>
                        <div class=\"pull-right\">
                            <a href=\"/lucky/signup\" class=\"link\"><i class=\"fa fa-caret-right\"></i> SIGN UP</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>

";
        // line 43
        echo "            <footer class=\"footer\">
                <div class=\"container\">
                    <div class=\"copyright\">&copy;
                        <script>document.write(new Date().getFullYear())</script>, Designed by
                        <a href=\"#\" target=\"_blank\">After Laughter</a>. Coded by 
                        <a href=\"#\" target=\"_blank\">After Laughter</a>.
                    </div>
                </div>
            </footer>    
        </div>
    </div>

";
    }

    public function getTemplateName()
    {
        return ":body:body_signin.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 43,  78 => 32,  71 => 28,  67 => 26,  62 => 23,  58 => 21,  51 => 16,  41 => 9,  38 => 8,  35 => 6,  31 => 3,  28 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", ":body:body_signin.html.twig", "/home/babypandalabs/microblog/app/Resources/views/body/body_signin.html.twig");
    }
}
