<?php

/* :core:header.html.twig */
class __TwigTemplate_dae860416a3cc64d256028ec128fe44fc394563342690bc06ecce3b373dff5d3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
     <head>    
        <meta charset=\"UTF-8\" />
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\" />
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />       
        
        <title>
            ";
        // line 9
        $this->displayBlock('title', $context, $blocks);
        // line 10
        echo "        </title>
        
        ";
        // line 12
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 54
        echo "  
    </head>

";
    }

    // line 9
    public function block_title($context, array $blocks = array())
    {
        echo "FoxHound";
    }

    // line 12
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 13
        echo "            <meta charset=\"utf-8\" />
            <link rel=\"stylesheet\" rel=\"apple-touch-icon\" sizes=\"76x76\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/now-ui-kit-v1.1.0/assets/img/apple-icon.png"), "html", null, true);
        echo "\" />
            <link rel=\"stylesheet\" rel=\"icon\" type=\"image/png\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/now-ui-kit-v1.1.0/assets/img/favicon.png"), "html", null, true);
        echo "\" />
            <link rel=\"stylesheet\" href=\"https://fonts.googleapis.com/css?family=Montserrat:400,700,200\" />
            <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css\" />
            <link rel=\"stylesheet\" href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/now-ui-kit-v1.1.0/assets/css/bootstrap.min.css"), "html", null, true);
        echo "\" />
            <link rel=\"stylesheet\" href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/now-ui-kit-v1.1.0/assets/css/now-ui-kit.css"), "html", null, true);
        echo "\" />
            <link rel=\"stylesheet\" href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/now-ui-kit-v1.1.0/assets/css/demo.css"), "html", null, true);
        echo "\" />
            <style>
                .txarea {
                    width: 100%;
                    font-size: 15px;
                    padding-left: 10px;
                    padding-top: 10px;
                    padding-right: 10px;
                    padding-bottom: 0px;
                    border-bottom-width: 2px;
                    border-style: solid;
                }
                .distxarea {
                    width: 100%;
                    font-size: 15px;
                    padding-left: 10px;
                    padding-top: 10px;
                    padding-right: 10px;
                    padding-bottom: 0px;
                    border-bottom-width: 2px;
                    border-style: solid;
                }
                .txbox {
                    width: 100%;
                    font-size: 15px;
                    padding-left: 10px;
                    padding-top: 10px;
                    padding-right: 10px;
                    padding-bottom: 13px;
                    border-bottom-width: 1px;
                    border-bottom-top: 2px;
                    border-style: solid;
                }
            </style>
        ";
    }

    public function getTemplateName()
    {
        return ":core:header.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  76 => 20,  72 => 19,  68 => 18,  62 => 15,  58 => 14,  55 => 13,  52 => 12,  46 => 9,  39 => 54,  37 => 12,  33 => 10,  31 => 9,  21 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", ":core:header.html.twig", "/home/babypandalabs/microblog/app/Resources/views/core/header.html.twig");
    }
}
