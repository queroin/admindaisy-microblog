<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_540208acb6eb8e68f2ef3e007d7996a1eb047223c9fe99bdc16804a2f1c9db94 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_572b639a5ed2e58fc0a02f75b43f5a1dfbb8823a5af6af9917966f13075eff98 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_572b639a5ed2e58fc0a02f75b43f5a1dfbb8823a5af6af9917966f13075eff98->enter($__internal_572b639a5ed2e58fc0a02f75b43f5a1dfbb8823a5af6af9917966f13075eff98_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $__internal_ace49e91b793014cf010c09a9001ca36de3817a2545db0744cf0839222f37ef7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ace49e91b793014cf010c09a9001ca36de3817a2545db0744cf0839222f37ef7->enter($__internal_ace49e91b793014cf010c09a9001ca36de3817a2545db0744cf0839222f37ef7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_572b639a5ed2e58fc0a02f75b43f5a1dfbb8823a5af6af9917966f13075eff98->leave($__internal_572b639a5ed2e58fc0a02f75b43f5a1dfbb8823a5af6af9917966f13075eff98_prof);

        
        $__internal_ace49e91b793014cf010c09a9001ca36de3817a2545db0744cf0839222f37ef7->leave($__internal_ace49e91b793014cf010c09a9001ca36de3817a2545db0744cf0839222f37ef7_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_86198e900298f8528cd8f9dfdff8401dae9a44d81e6708fa7a2c855a71c87372 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_86198e900298f8528cd8f9dfdff8401dae9a44d81e6708fa7a2c855a71c87372->enter($__internal_86198e900298f8528cd8f9dfdff8401dae9a44d81e6708fa7a2c855a71c87372_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_1a59195f22e80ae1591f5276d8dae5dd61f748c8cdde7f4f8d55dd1b6cdbefae = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1a59195f22e80ae1591f5276d8dae5dd61f748c8cdde7f4f8d55dd1b6cdbefae->enter($__internal_1a59195f22e80ae1591f5276d8dae5dd61f748c8cdde7f4f8d55dd1b6cdbefae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
";
        
        $__internal_1a59195f22e80ae1591f5276d8dae5dd61f748c8cdde7f4f8d55dd1b6cdbefae->leave($__internal_1a59195f22e80ae1591f5276d8dae5dd61f748c8cdde7f4f8d55dd1b6cdbefae_prof);

        
        $__internal_86198e900298f8528cd8f9dfdff8401dae9a44d81e6708fa7a2c855a71c87372->leave($__internal_86198e900298f8528cd8f9dfdff8401dae9a44d81e6708fa7a2c855a71c87372_prof);

    }

    // line 136
    public function block_title($context, array $blocks = array())
    {
        $__internal_69cf81477b9a8d88f175e9a3c96f9d06ea8f0914707ecf2ddc7e5f61a2743903 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_69cf81477b9a8d88f175e9a3c96f9d06ea8f0914707ecf2ddc7e5f61a2743903->enter($__internal_69cf81477b9a8d88f175e9a3c96f9d06ea8f0914707ecf2ddc7e5f61a2743903_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_5a2e95dee42c6f4cef43dcf5ec14d131cb3607ac94f3cb0c7c5127945f507811 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5a2e95dee42c6f4cef43dcf5ec14d131cb3607ac94f3cb0c7c5127945f507811->enter($__internal_5a2e95dee42c6f4cef43dcf5ec14d131cb3607ac94f3cb0c7c5127945f507811_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 137
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_5a2e95dee42c6f4cef43dcf5ec14d131cb3607ac94f3cb0c7c5127945f507811->leave($__internal_5a2e95dee42c6f4cef43dcf5ec14d131cb3607ac94f3cb0c7c5127945f507811_prof);

        
        $__internal_69cf81477b9a8d88f175e9a3c96f9d06ea8f0914707ecf2ddc7e5f61a2743903->leave($__internal_69cf81477b9a8d88f175e9a3c96f9d06ea8f0914707ecf2ddc7e5f61a2743903_prof);

    }

    // line 140
    public function block_body($context, array $blocks = array())
    {
        $__internal_4a75517d0772d553f7c2ef2689b212e12b548659ba790b3559f1f480528e1974 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4a75517d0772d553f7c2ef2689b212e12b548659ba790b3559f1f480528e1974->enter($__internal_4a75517d0772d553f7c2ef2689b212e12b548659ba790b3559f1f480528e1974_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_5491483220f1b1a8f68cc0d88743da88a8fb599ebf2089cdc00ca0a6b96391bb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5491483220f1b1a8f68cc0d88743da88a8fb599ebf2089cdc00ca0a6b96391bb->enter($__internal_5491483220f1b1a8f68cc0d88743da88a8fb599ebf2089cdc00ca0a6b96391bb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 141
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 141)->display($context);
        
        $__internal_5491483220f1b1a8f68cc0d88743da88a8fb599ebf2089cdc00ca0a6b96391bb->leave($__internal_5491483220f1b1a8f68cc0d88743da88a8fb599ebf2089cdc00ca0a6b96391bb_prof);

        
        $__internal_4a75517d0772d553f7c2ef2689b212e12b548659ba790b3559f1f480528e1974->leave($__internal_4a75517d0772d553f7c2ef2689b212e12b548659ba790b3559f1f480528e1974_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  226 => 141,  217 => 140,  200 => 137,  191 => 136,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "@Twig/Exception/exception_full.html.twig", "/home/babypandalabs/microblog/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception_full.html.twig");
    }
}
