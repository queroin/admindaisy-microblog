<?php

/* body/body_home.html.twig */
class __TwigTemplate_3c2af1c897db283e0e058a2e51525b59e25dd7ab72cf085a5a1fa2f114ca0f59 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "body/body_home.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_87920862c11e1b85cc222cd183903be47c16c0b6a2c7b3800bcc59bceb8cfe3b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_87920862c11e1b85cc222cd183903be47c16c0b6a2c7b3800bcc59bceb8cfe3b->enter($__internal_87920862c11e1b85cc222cd183903be47c16c0b6a2c7b3800bcc59bceb8cfe3b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "body/body_home.html.twig"));

        $__internal_e9a3abda7cf4661b7e207f368d00940d3f22d9f893f93faaef4658ea37fe6d88 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e9a3abda7cf4661b7e207f368d00940d3f22d9f893f93faaef4658ea37fe6d88->enter($__internal_e9a3abda7cf4661b7e207f368d00940d3f22d9f893f93faaef4658ea37fe6d88_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "body/body_home.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_87920862c11e1b85cc222cd183903be47c16c0b6a2c7b3800bcc59bceb8cfe3b->leave($__internal_87920862c11e1b85cc222cd183903be47c16c0b6a2c7b3800bcc59bceb8cfe3b_prof);

        
        $__internal_e9a3abda7cf4661b7e207f368d00940d3f22d9f893f93faaef4658ea37fe6d88->leave($__internal_e9a3abda7cf4661b7e207f368d00940d3f22d9f893f93faaef4658ea37fe6d88_prof);

    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        $__internal_22a44a78a5671cb8a460b45403f66f6e0ccac122deb448be7ac488ba18d65d22 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_22a44a78a5671cb8a460b45403f66f6e0ccac122deb448be7ac488ba18d65d22->enter($__internal_22a44a78a5671cb8a460b45403f66f6e0ccac122deb448be7ac488ba18d65d22_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_90eb10d25102e7053d88a1bed66983b01411d679e9352299737d64062ff49279 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_90eb10d25102e7053d88a1bed66983b01411d679e9352299737d64062ff49279->enter($__internal_90eb10d25102e7053d88a1bed66983b01411d679e9352299737d64062ff49279_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <div class=\"wrapper\">
        <div class=\"page-header clear-filter\" filter-color=\"orange\">
            <div class=\"page-header-image\" data-parallax=\"true\" style=\"background-image:url(";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/now-ui-kit-v1.1.0/assets//img/bgg3.jpg"), "html", null, true);
        echo ");\"></div>
            
            <div class=\"container\">
                <div class=\"content-center brand\">
                    <img class=\"n-logo\" src=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/now-ui-kit-v1.1.0/assets/img/blog.png"), "html", null, true);
        echo "\" alt=\"\" style=\"padding-top: 80px;\">
                    <h1 class=\"h1 seo\">BLOG</h1>
                    <h3>Create. Inspire. Share. Connect.</h3>
                    <div style=\"text-align:center;\"><br><br><br>
                    <a href=\"/post/new\" class=\"btn btn-primary btn-round btn-lg btn-block\">WRITE A BLOG</a>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"section section-team text-center\">
            <div class=\"container\">
                <h2 class=\"title\">Team</h2><br>
                <div class=\"team\">
                    <div class=\"row\">
                        <div class=\"col-md-4\">
                            <div class=\"team-player\">
                                <img src=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/now-ui-kit-v1.1.0/assets//img/team_quer.jpg"), "html", null, true);
        echo "\" alt=\"Thumbnail Image\" class=\"rounded-circle img-fluid img-raised\" style=\"height: 150px; width: 150px;\">
                                <h4 class=\"title\">Quer Laconico</h4>
                                
                                
                                <p class=\"category text-primary\">Programmer</p>
                            </div>
                        </div>                       
                        <div class=\"col-md-4\">
                            <div class=\"team-player\">
                                <img src=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/now-ui-kit-v1.1.0/assets//img/team_kyle.jpg"), "html", null, true);
        echo "\" alt=\"Thumbnail Image\" class=\"rounded-circle img-fluid img-raised\" style=\"height: 150px; width: 150px;\">
                                <h4 class=\"title\">Kyle Avedana</h4>
                                <p class=\"category text-primary\">Programmer</p>
                            </div>
                        </div>
                        <div class=\"col-md-4\">
                            <div class=\"team-player\">
                                <img src=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/now-ui-kit-v1.1.0/assets//img/team_mae.jpg"), "html", null, true);
        echo "\" alt=\"Thumbnail Image\" class=\"rounded-circle img-fluid img-raised\" style=\"height: 150px; width: 150px;\">
                                <h4 class=\"title\">Mae Sibal</h4>
                                <p class=\"category text-primary\">Programmer</p>
                            </div>
                        </div>
                        <div class=\"col-md-4\">
                            <div class=\"team-player\">
                                <img src=\"";
        // line 49
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/now-ui-kit-v1.1.0/assets//img/team_gra.jpg"), "html", null, true);
        echo "\" alt=\"Thumbnail Image\" class=\"rounded-circle img-fluid img-raised\" style=\"height: 150px; width: 150px;\">
                                <h4 class=\"title\">Gra Ayun</h4>
                                <p class=\"category text-primary\">Web Designer</p>
                            </div>
                        </div>
                        <div class=\"col-md-4\">
                            <div class=\"team-player\">
                                <img src=\"";
        // line 56
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/now-ui-kit-v1.1.0/assets//img/team_era.jpg"), "html", null, true);
        echo "\" alt=\"Thumbnail Image\" class=\"rounded-circle img-fluid img-raised\" style=\"height: 150px; width: 150px;\">
                                <h4 class=\"title\">Era Vergara</h4>
                                <p class=\"category text-primary\">Web Designer</p>
                            </div>
                        </div>
                        <div class=\"col-md-4\">
                            <div class=\"team-player\">
                                <img src=\"";
        // line 63
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/now-ui-kit-v1.1.0/assets//img/team_trags.jpg"), "html", null, true);
        echo "\" alt=\"Thumbnail Image\" class=\"rounded-circle img-fluid img-raised\" style=\"height: 150px; width: 150px;\">
                                <h4 class=\"title\">Lei Tragura</h4>
                                <p class=\"category text-primary\">Web Designer</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
  

";
        
        $__internal_90eb10d25102e7053d88a1bed66983b01411d679e9352299737d64062ff49279->leave($__internal_90eb10d25102e7053d88a1bed66983b01411d679e9352299737d64062ff49279_prof);

        
        $__internal_22a44a78a5671cb8a460b45403f66f6e0ccac122deb448be7ac488ba18d65d22->leave($__internal_22a44a78a5671cb8a460b45403f66f6e0ccac122deb448be7ac488ba18d65d22_prof);

    }

    public function getTemplateName()
    {
        return "body/body_home.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  131 => 63,  121 => 56,  111 => 49,  101 => 42,  91 => 35,  79 => 26,  60 => 10,  53 => 6,  49 => 4,  40 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}
{% block body%}
{# Body #}
    <div class=\"wrapper\">
        <div class=\"page-header clear-filter\" filter-color=\"orange\">
            <div class=\"page-header-image\" data-parallax=\"true\" style=\"background-image:url({{asset('assets/now-ui-kit-v1.1.0/assets//img/bgg3.jpg')}});\"></div>
            
            <div class=\"container\">
                <div class=\"content-center brand\">
                    <img class=\"n-logo\" src=\"{{asset('assets/now-ui-kit-v1.1.0/assets/img/blog.png')}}\" alt=\"\" style=\"padding-top: 80px;\">
                    <h1 class=\"h1 seo\">BLOG</h1>
                    <h3>Create. Inspire. Share. Connect.</h3>
                    <div style=\"text-align:center;\"><br><br><br>
                    <a href=\"/post/new\" class=\"btn btn-primary btn-round btn-lg btn-block\">WRITE A BLOG</a>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"section section-team text-center\">
            <div class=\"container\">
                <h2 class=\"title\">Team</h2><br>
                <div class=\"team\">
                    <div class=\"row\">
                        <div class=\"col-md-4\">
                            <div class=\"team-player\">
                                <img src=\"{{asset('assets/now-ui-kit-v1.1.0/assets//img/team_quer.jpg')}}\" alt=\"Thumbnail Image\" class=\"rounded-circle img-fluid img-raised\" style=\"height: 150px; width: 150px;\">
                                <h4 class=\"title\">Quer Laconico</h4>
                                
                                
                                <p class=\"category text-primary\">Programmer</p>
                            </div>
                        </div>                       
                        <div class=\"col-md-4\">
                            <div class=\"team-player\">
                                <img src=\"{{asset('assets/now-ui-kit-v1.1.0/assets//img/team_kyle.jpg')}}\" alt=\"Thumbnail Image\" class=\"rounded-circle img-fluid img-raised\" style=\"height: 150px; width: 150px;\">
                                <h4 class=\"title\">Kyle Avedana</h4>
                                <p class=\"category text-primary\">Programmer</p>
                            </div>
                        </div>
                        <div class=\"col-md-4\">
                            <div class=\"team-player\">
                                <img src=\"{{asset('assets/now-ui-kit-v1.1.0/assets//img/team_mae.jpg')}}\" alt=\"Thumbnail Image\" class=\"rounded-circle img-fluid img-raised\" style=\"height: 150px; width: 150px;\">
                                <h4 class=\"title\">Mae Sibal</h4>
                                <p class=\"category text-primary\">Programmer</p>
                            </div>
                        </div>
                        <div class=\"col-md-4\">
                            <div class=\"team-player\">
                                <img src=\"{{asset('assets/now-ui-kit-v1.1.0/assets//img/team_gra.jpg')}}\" alt=\"Thumbnail Image\" class=\"rounded-circle img-fluid img-raised\" style=\"height: 150px; width: 150px;\">
                                <h4 class=\"title\">Gra Ayun</h4>
                                <p class=\"category text-primary\">Web Designer</p>
                            </div>
                        </div>
                        <div class=\"col-md-4\">
                            <div class=\"team-player\">
                                <img src=\"{{asset('assets/now-ui-kit-v1.1.0/assets//img/team_era.jpg')}}\" alt=\"Thumbnail Image\" class=\"rounded-circle img-fluid img-raised\" style=\"height: 150px; width: 150px;\">
                                <h4 class=\"title\">Era Vergara</h4>
                                <p class=\"category text-primary\">Web Designer</p>
                            </div>
                        </div>
                        <div class=\"col-md-4\">
                            <div class=\"team-player\">
                                <img src=\"{{asset('assets/now-ui-kit-v1.1.0/assets//img/team_trags.jpg')}}\" alt=\"Thumbnail Image\" class=\"rounded-circle img-fluid img-raised\" style=\"height: 150px; width: 150px;\">
                                <h4 class=\"title\">Lei Tragura</h4>
                                <p class=\"category text-primary\">Web Designer</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
  

{% endblock %}

", "body/body_home.html.twig", "/home/babypandalabs/microblog/app/Resources/views/body/body_home.html.twig");
    }
}
