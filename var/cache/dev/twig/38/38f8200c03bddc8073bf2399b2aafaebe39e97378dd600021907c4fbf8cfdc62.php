<?php

/* @WebProfiler/Icon/twig.svg */
class __TwigTemplate_b7300a2b81034dd793edf769786d744fedd5868e61652a541288f7a72a961a92 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c9d94ab7f0d40cbb536196fb83a37e297d5c01182e758c40cc0130f9e5453ec8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c9d94ab7f0d40cbb536196fb83a37e297d5c01182e758c40cc0130f9e5453ec8->enter($__internal_c9d94ab7f0d40cbb536196fb83a37e297d5c01182e758c40cc0130f9e5453ec8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Icon/twig.svg"));

        $__internal_e21741b4a7d264e7274f5bf0f5e1c5de097b0430c64a3c3832e6eef8a260b721 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e21741b4a7d264e7274f5bf0f5e1c5de097b0430c64a3c3832e6eef8a260b721->enter($__internal_e21741b4a7d264e7274f5bf0f5e1c5de097b0430c64a3c3832e6eef8a260b721_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Icon/twig.svg"));

        // line 1
        echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
<path fill=\"#AAAAAA\" d=\"M20.1,1H3.9C2.3,1,1,2.3,1,3.9v16.3C1,21.7,2.3,23,3.9,23h16.3c1.6,0,2.9-1.3,2.9-2.9V3.9
    C23,2.3,21.7,1,20.1,1z M21,20.1c0,0.5-0.4,0.9-0.9,0.9H3.9C3.4,21,3,20.6,3,20.1V3.9C3,3.4,3.4,3,3.9,3h16.3C20.6,3,21,3.4,21,3.9
    V20.1z M5,5h14v3H5V5z M5,10h3v9H5V10z M10,10h9v9h-9V10z\"/>
</svg>
";
        
        $__internal_c9d94ab7f0d40cbb536196fb83a37e297d5c01182e758c40cc0130f9e5453ec8->leave($__internal_c9d94ab7f0d40cbb536196fb83a37e297d5c01182e758c40cc0130f9e5453ec8_prof);

        
        $__internal_e21741b4a7d264e7274f5bf0f5e1c5de097b0430c64a3c3832e6eef8a260b721->leave($__internal_e21741b4a7d264e7274f5bf0f5e1c5de097b0430c64a3c3832e6eef8a260b721_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Icon/twig.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
<path fill=\"#AAAAAA\" d=\"M20.1,1H3.9C2.3,1,1,2.3,1,3.9v16.3C1,21.7,2.3,23,3.9,23h16.3c1.6,0,2.9-1.3,2.9-2.9V3.9
    C23,2.3,21.7,1,20.1,1z M21,20.1c0,0.5-0.4,0.9-0.9,0.9H3.9C3.4,21,3,20.6,3,20.1V3.9C3,3.4,3.4,3,3.9,3h16.3C20.6,3,21,3.4,21,3.9
    V20.1z M5,5h14v3H5V5z M5,10h3v9H5V10z M10,10h9v9h-9V10z\"/>
</svg>
", "@WebProfiler/Icon/twig.svg", "/home/babypandalabs/microblog/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Icon/twig.svg");
    }
}
