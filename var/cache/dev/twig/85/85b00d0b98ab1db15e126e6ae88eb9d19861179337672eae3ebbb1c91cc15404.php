<?php

/* @FOSUser/Profile/edit.html.twig */
class __TwigTemplate_d0a69ce79b3d71a87eb187f056f3ef92b6d0b426395d332d7034acc9b3c30ce9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Profile/edit.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_40b911ed5393ff84cd9b8ad3b0248727b88a053b6c1f5a375a74b4d5db4f6228 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_40b911ed5393ff84cd9b8ad3b0248727b88a053b6c1f5a375a74b4d5db4f6228->enter($__internal_40b911ed5393ff84cd9b8ad3b0248727b88a053b6c1f5a375a74b4d5db4f6228_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Profile/edit.html.twig"));

        $__internal_3dd5598c7b0b25f23058e2d3ea08ce39451c136fbc2795a88d1e9aac20a89505 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3dd5598c7b0b25f23058e2d3ea08ce39451c136fbc2795a88d1e9aac20a89505->enter($__internal_3dd5598c7b0b25f23058e2d3ea08ce39451c136fbc2795a88d1e9aac20a89505_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Profile/edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_40b911ed5393ff84cd9b8ad3b0248727b88a053b6c1f5a375a74b4d5db4f6228->leave($__internal_40b911ed5393ff84cd9b8ad3b0248727b88a053b6c1f5a375a74b4d5db4f6228_prof);

        
        $__internal_3dd5598c7b0b25f23058e2d3ea08ce39451c136fbc2795a88d1e9aac20a89505->leave($__internal_3dd5598c7b0b25f23058e2d3ea08ce39451c136fbc2795a88d1e9aac20a89505_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_8552e6785cdf9ce0e9b8a697967198d6941d25ad3afc1e808cf43addd234dc5c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8552e6785cdf9ce0e9b8a697967198d6941d25ad3afc1e808cf43addd234dc5c->enter($__internal_8552e6785cdf9ce0e9b8a697967198d6941d25ad3afc1e808cf43addd234dc5c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_ca66bfb678609da015ef85e41c190af7cb4cad9371e7c2b34a4ed95851a6a7df = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ca66bfb678609da015ef85e41c190af7cb4cad9371e7c2b34a4ed95851a6a7df->enter($__internal_ca66bfb678609da015ef85e41c190af7cb4cad9371e7c2b34a4ed95851a6a7df_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Profile/edit_content.html.twig", "@FOSUser/Profile/edit.html.twig", 4)->display($context);
        
        $__internal_ca66bfb678609da015ef85e41c190af7cb4cad9371e7c2b34a4ed95851a6a7df->leave($__internal_ca66bfb678609da015ef85e41c190af7cb4cad9371e7c2b34a4ed95851a6a7df_prof);

        
        $__internal_8552e6785cdf9ce0e9b8a697967198d6941d25ad3afc1e808cf43addd234dc5c->leave($__internal_8552e6785cdf9ce0e9b8a697967198d6941d25ad3afc1e808cf43addd234dc5c_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Profile/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Profile/edit_content.html.twig\" %}
{% endblock fos_user_content %}
", "@FOSUser/Profile/edit.html.twig", "/home/babypandalabs/microblog/app/Resources/FOSUserBundle/views/Profile/edit.html.twig");
    }
}
