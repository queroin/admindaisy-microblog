<?php

/* @FOSUser/Registration/register_content.html.twig */
class __TwigTemplate_3df68f4929b22a0873ca38dad9cd7e5e9c0fe916cddc69d4f52300959b4b77af extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 3
        $this->parent = $this->loadTemplate("base.html.twig", "@FOSUser/Registration/register_content.html.twig", 3);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1fcac6434f58277a61336d8e4004a0fe74a7236f8654188273cd8ec814b7e9ee = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1fcac6434f58277a61336d8e4004a0fe74a7236f8654188273cd8ec814b7e9ee->enter($__internal_1fcac6434f58277a61336d8e4004a0fe74a7236f8654188273cd8ec814b7e9ee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/register_content.html.twig"));

        $__internal_7c71d816d79c197fa61b2b3100b5391453c89c850314bbe691e0c48392320a68 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7c71d816d79c197fa61b2b3100b5391453c89c850314bbe691e0c48392320a68->enter($__internal_7c71d816d79c197fa61b2b3100b5391453c89c850314bbe691e0c48392320a68_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/register_content.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_1fcac6434f58277a61336d8e4004a0fe74a7236f8654188273cd8ec814b7e9ee->leave($__internal_1fcac6434f58277a61336d8e4004a0fe74a7236f8654188273cd8ec814b7e9ee_prof);

        
        $__internal_7c71d816d79c197fa61b2b3100b5391453c89c850314bbe691e0c48392320a68->leave($__internal_7c71d816d79c197fa61b2b3100b5391453c89c850314bbe691e0c48392320a68_prof);

    }

    // line 4
    public function block_body($context, array $blocks = array())
    {
        $__internal_c20ade9df337a03daab4f3d262acebd5f76272dc3c6854511765d194432a7bfd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c20ade9df337a03daab4f3d262acebd5f76272dc3c6854511765d194432a7bfd->enter($__internal_c20ade9df337a03daab4f3d262acebd5f76272dc3c6854511765d194432a7bfd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_a0f4529c4e69680143d320930b39112c12491d9f3a443aaa04a4f6af8b9bc950 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a0f4529c4e69680143d320930b39112c12491d9f3a443aaa04a4f6af8b9bc950->enter($__internal_a0f4529c4e69680143d320930b39112c12491d9f3a443aaa04a4f6af8b9bc950_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo "
<body>
";
        // line 9
        echo "
";
        // line 11
        echo "    <div class=\"page-header\" filter-color=\"orange\">
        <div class=\"page-header-image\" style=\"background-image:url(";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/now-ui-kit-v1.1.0/assets/img/login.jpg"), "html", null, true);
        echo ")\"></div>
        <div class=\"container\">
            <div class=\"col-md-4 content-center\">
                <div class=\"card card-signup\" data-background-color=\"orange\">
                   \t<div class=\"header text-center\">
                        <h4 class=\"title title-up\">Sign Up</h4>
                   \t</div>
                    ";
        // line 19
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start', array("method" => "post", "action" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_registration_register"), "attr" => array("class" => "fos_user_registration_register")));
        echo "

                    <div class=\"card-body\">
                    \t
\t\t\t\t\t\t";
        // line 24
        echo "                        <div class=\"input-group form-group-no-border\">
                            <span class=\"input-group-addon\"><i class=\"fa fa-user\"></i></span>
                         \t";
        // line 26
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "fname", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "First Name")));
        echo "
                        </div>



                        ";
        // line 32
        echo "                        <div class=\"input-group form-group-no-border\">
                            <span class=\"input-group-addon\"><i class=\"fa fa-user\"></i></span>
                         \t";
        // line 34
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "lname", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "last Name")));
        echo "
                        </div>
                        ";
        // line 37
        echo "                        <div class=\"input-group form-group-no-border\">
                            <span class=\"input-group-addon\"><i class=\"fa fa-user\"></i></span>
                         \t";
        // line 39
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "username", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Username")));
        echo "
                        </div>
                        ";
        // line 42
        echo "
                        <div class=\"input-group form-group-no-border\">
                            <span class=\"input-group-addon\"><i class=\"fa fa-at\"></i></span>
                         \t";
        // line 45
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "your_email@email.com")));
        echo "
                        </div>
                        ";
        // line 47
        echo "  
                        <div class=\"input-group form-group-no-border\">
                            <span class=\"input-group-addon\"><i class=\"fa fa-lock\"></i></span>
                         \t ";
        // line 50
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "plainPassword", array()), "first", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Password")));
        echo "
                        </div>

                        ";
        // line 53
        echo "  
                        <div class=\"input-group form-group-no-border\">
                            <span class=\"input-group-addon\"><i class=\"fa fa-lock\"></i></span>
                             ";
        // line 56
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "plainPassword", array()), "second", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Confirm Password")));
        echo "
                        </div>
                       
                        
                        

\t\t\t\t\t\t<div class=\"footer text-center\">
                            <input type=\"submit\" id=\"_submit\" name=\"_submit\" value=\"";
        // line 63
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\" class=\"btn btn-neutral btn-round btn-lg\" />
                        </div>
                        ";
        // line 65
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
                        </form>

                \t</div>
            \t</div>
        \t</div>
  
    \t</div>
    </div>

";
        
        $__internal_a0f4529c4e69680143d320930b39112c12491d9f3a443aaa04a4f6af8b9bc950->leave($__internal_a0f4529c4e69680143d320930b39112c12491d9f3a443aaa04a4f6af8b9bc950_prof);

        
        $__internal_c20ade9df337a03daab4f3d262acebd5f76272dc3c6854511765d194432a7bfd->leave($__internal_c20ade9df337a03daab4f3d262acebd5f76272dc3c6854511765d194432a7bfd_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Registration/register_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  147 => 65,  142 => 63,  132 => 56,  127 => 53,  121 => 50,  116 => 47,  111 => 45,  106 => 42,  101 => 39,  97 => 37,  92 => 34,  88 => 32,  80 => 26,  76 => 24,  69 => 19,  59 => 12,  56 => 11,  53 => 9,  49 => 5,  40 => 4,  11 => 3,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("

{% extends 'base.html.twig' %}
{% block body%}

<body>
{# Header #}
{% trans_default_domain 'FOSUserBundle' %}

{# Body #}
    <div class=\"page-header\" filter-color=\"orange\">
        <div class=\"page-header-image\" style=\"background-image:url({{asset('assets/now-ui-kit-v1.1.0/assets/img/login.jpg')}})\"></div>
        <div class=\"container\">
            <div class=\"col-md-4 content-center\">
                <div class=\"card card-signup\" data-background-color=\"orange\">
                   \t<div class=\"header text-center\">
                        <h4 class=\"title title-up\">Sign Up</h4>
                   \t</div>
                    {{ form_start(form, {'method': 'post', 'action': path('fos_user_registration_register'), 'attr': {'class': 'fos_user_registration_register'}}) }}

                    <div class=\"card-body\">
                    \t
\t\t\t\t\t\t{# fname #}
                        <div class=\"input-group form-group-no-border\">
                            <span class=\"input-group-addon\"><i class=\"fa fa-user\"></i></span>
                         \t{{ form_widget(form.fname, { 'attr': {'class': 'form-control', 'placeholder': 'First Name'} }) }}
                        </div>



                        {# lname #}
                        <div class=\"input-group form-group-no-border\">
                            <span class=\"input-group-addon\"><i class=\"fa fa-user\"></i></span>
                         \t{{ form_widget(form.lname, { 'attr': {'class': 'form-control', 'placeholder': 'last Name'} }) }}
                        </div>
                        {# username #}
                        <div class=\"input-group form-group-no-border\">
                            <span class=\"input-group-addon\"><i class=\"fa fa-user\"></i></span>
                         \t{{ form_widget(form.username, { 'attr': {'class': 'form-control', 'placeholder': 'Username'} }) }}
                        </div>
                        {# email #}

                        <div class=\"input-group form-group-no-border\">
                            <span class=\"input-group-addon\"><i class=\"fa fa-at\"></i></span>
                         \t{{ form_widget(form.email, { 'attr': {'class': 'form-control', 'placeholder': 'your_email@email.com'} }) }}
                        </div>
                        {# password #}  
                        <div class=\"input-group form-group-no-border\">
                            <span class=\"input-group-addon\"><i class=\"fa fa-lock\"></i></span>
                         \t {{ form_widget(form.plainPassword.first, { 'attr': {'class': 'form-control', 'placeholder': 'Password'} }) }}
                        </div>

                        {# confirm password #}  
                        <div class=\"input-group form-group-no-border\">
                            <span class=\"input-group-addon\"><i class=\"fa fa-lock\"></i></span>
                             {{ form_widget(form.plainPassword.second, { 'attr': {'class': 'form-control', 'placeholder': 'Confirm Password'} }) }}
                        </div>
                       
                        
                        

\t\t\t\t\t\t<div class=\"footer text-center\">
                            <input type=\"submit\" id=\"_submit\" name=\"_submit\" value=\"{{ 'registration.submit'|trans }}\" class=\"btn btn-neutral btn-round btn-lg\" />
                        </div>
                        {{ form_end(form) }}
                        </form>

                \t</div>
            \t</div>
        \t</div>
  
    \t</div>
    </div>

{% endblock %}", "@FOSUser/Registration/register_content.html.twig", "/home/babypandalabs/microblog/app/Resources/FOSUserBundle/views/Registration/register_content.html.twig");
    }
}
