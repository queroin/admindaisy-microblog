<?php

/* body/body_footer.html.twig */
class __TwigTemplate_5a9dc1540abc0d9ed2b97eea7f64bf6205de64c3f503631aa92255d2c32a15c7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'body_footer' => array($this, 'block_body_footer'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d5484c0deff54f075c7fc09a459a6b76c19e4aa485d831a85852889e6143375f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d5484c0deff54f075c7fc09a459a6b76c19e4aa485d831a85852889e6143375f->enter($__internal_d5484c0deff54f075c7fc09a459a6b76c19e4aa485d831a85852889e6143375f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "body/body_footer.html.twig"));

        $__internal_290558a3f7843f41f79a10847bff028e0715d27b680ce9b64af9db4242c00f8e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_290558a3f7843f41f79a10847bff028e0715d27b680ce9b64af9db4242c00f8e->enter($__internal_290558a3f7843f41f79a10847bff028e0715d27b680ce9b64af9db4242c00f8e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "body/body_footer.html.twig"));

        // line 1
        $this->displayBlock('body_footer', $context, $blocks);
        
        $__internal_d5484c0deff54f075c7fc09a459a6b76c19e4aa485d831a85852889e6143375f->leave($__internal_d5484c0deff54f075c7fc09a459a6b76c19e4aa485d831a85852889e6143375f_prof);

        
        $__internal_290558a3f7843f41f79a10847bff028e0715d27b680ce9b64af9db4242c00f8e->leave($__internal_290558a3f7843f41f79a10847bff028e0715d27b680ce9b64af9db4242c00f8e_prof);

    }

    public function block_body_footer($context, array $blocks = array())
    {
        $__internal_0e05a057ffc2831e3fd8fe3bdfcb873aa116b2bcc68d9f88c0927493f361093b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0e05a057ffc2831e3fd8fe3bdfcb873aa116b2bcc68d9f88c0927493f361093b->enter($__internal_0e05a057ffc2831e3fd8fe3bdfcb873aa116b2bcc68d9f88c0927493f361093b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_footer"));

        $__internal_410093a0cb666916d735b4f33a4626f42d427b08e196abba9063e6ef8a04984e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_410093a0cb666916d735b4f33a4626f42d427b08e196abba9063e6ef8a04984e->enter($__internal_410093a0cb666916d735b4f33a4626f42d427b08e196abba9063e6ef8a04984e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_footer"));

        // line 2
        echo "      ";
        // line 3
        echo "            <footer class=\"footer\">
                <div class=\"container\">
                    <div class=\"copyright\">&copy;
                        <script>document.write(new Date().getFullYear())</script>, Submitted by
                        <a href=\"#\" target=\"_blank\">LATEST GROUP</a>. Required project for  
                        <a href=\"#\" target=\"_blank\">BSIT ELECTIVE 3</a>.
                    </div>
                </div>
            </footer>    
        </div>
";
        
        $__internal_410093a0cb666916d735b4f33a4626f42d427b08e196abba9063e6ef8a04984e->leave($__internal_410093a0cb666916d735b4f33a4626f42d427b08e196abba9063e6ef8a04984e_prof);

        
        $__internal_0e05a057ffc2831e3fd8fe3bdfcb873aa116b2bcc68d9f88c0927493f361093b->leave($__internal_0e05a057ffc2831e3fd8fe3bdfcb873aa116b2bcc68d9f88c0927493f361093b_prof);

    }

    public function getTemplateName()
    {
        return "body/body_footer.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  46 => 3,  44 => 2,  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% block body_footer %}
      {# Footer #}
            <footer class=\"footer\">
                <div class=\"container\">
                    <div class=\"copyright\">&copy;
                        <script>document.write(new Date().getFullYear())</script>, Submitted by
                        <a href=\"#\" target=\"_blank\">LATEST GROUP</a>. Required project for  
                        <a href=\"#\" target=\"_blank\">BSIT ELECTIVE 3</a>.
                    </div>
                </div>
            </footer>    
        </div>
{% endblock %}", "body/body_footer.html.twig", "/home/babypandalabs/microblog/app/Resources/views/body/body_footer.html.twig");
    }
}
