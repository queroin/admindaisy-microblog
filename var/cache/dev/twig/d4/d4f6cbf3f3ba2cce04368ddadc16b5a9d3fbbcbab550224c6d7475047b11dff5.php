<?php

/* @FOSUser/Security/login.html.twig */
class __TwigTemplate_ccbd89d1fae1ddb2fff9a5833b17989ffd6438ce746b1e7a05cf6559a9904c40 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Security/login.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_77b023afa4f9a0e6f2d998519c41848da486c8c05bb57886ef93405200b2c7b0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_77b023afa4f9a0e6f2d998519c41848da486c8c05bb57886ef93405200b2c7b0->enter($__internal_77b023afa4f9a0e6f2d998519c41848da486c8c05bb57886ef93405200b2c7b0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login.html.twig"));

        $__internal_6c2c5cad3b64d696d94c39c4ca9ec04fea9b3285edc3c085ae2d5a1e77b07157 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6c2c5cad3b64d696d94c39c4ca9ec04fea9b3285edc3c085ae2d5a1e77b07157->enter($__internal_6c2c5cad3b64d696d94c39c4ca9ec04fea9b3285edc3c085ae2d5a1e77b07157_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_77b023afa4f9a0e6f2d998519c41848da486c8c05bb57886ef93405200b2c7b0->leave($__internal_77b023afa4f9a0e6f2d998519c41848da486c8c05bb57886ef93405200b2c7b0_prof);

        
        $__internal_6c2c5cad3b64d696d94c39c4ca9ec04fea9b3285edc3c085ae2d5a1e77b07157->leave($__internal_6c2c5cad3b64d696d94c39c4ca9ec04fea9b3285edc3c085ae2d5a1e77b07157_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_8280367a72680216b41d66c78ab501a40fa418016e11bec0b0179bc50cb31048 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8280367a72680216b41d66c78ab501a40fa418016e11bec0b0179bc50cb31048->enter($__internal_8280367a72680216b41d66c78ab501a40fa418016e11bec0b0179bc50cb31048_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_32d1e560b69e43b884e09ececd233f1c67aeff1001371dd1947624b1e538dc6f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_32d1e560b69e43b884e09ececd233f1c67aeff1001371dd1947624b1e538dc6f->enter($__internal_32d1e560b69e43b884e09ececd233f1c67aeff1001371dd1947624b1e538dc6f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        echo "    ";
        echo twig_include($this->env, $context, "@FOSUser/Security/login_content.html.twig");
        echo "
";
        
        $__internal_32d1e560b69e43b884e09ececd233f1c67aeff1001371dd1947624b1e538dc6f->leave($__internal_32d1e560b69e43b884e09ececd233f1c67aeff1001371dd1947624b1e538dc6f_prof);

        
        $__internal_8280367a72680216b41d66c78ab501a40fa418016e11bec0b0179bc50cb31048->leave($__internal_8280367a72680216b41d66c78ab501a40fa418016e11bec0b0179bc50cb31048_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Security/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
    {{ include('@FOSUser/Security/login_content.html.twig') }}
{% endblock fos_user_content %}
", "@FOSUser/Security/login.html.twig", "/home/babypandalabs/microblog/app/Resources/FOSUserBundle/views/Security/login.html.twig");
    }
}
