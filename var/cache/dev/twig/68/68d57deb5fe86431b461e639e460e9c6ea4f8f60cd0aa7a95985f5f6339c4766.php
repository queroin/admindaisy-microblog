<?php

/* core/header.html.twig */
class __TwigTemplate_b2cac9c0974f4dec193571153d927ec0bf1aa10f2b2e3ec8ba54b6e53fbcea34 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bb6d79e85e7bb246de61da58869e2c23d8715922087626a375621fa67f27dd4a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bb6d79e85e7bb246de61da58869e2c23d8715922087626a375621fa67f27dd4a->enter($__internal_bb6d79e85e7bb246de61da58869e2c23d8715922087626a375621fa67f27dd4a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "core/header.html.twig"));

        $__internal_8a1697063851c1706e2ae49a5b2bc83253296e8a32d96b66e0b1f606ed31eebe = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8a1697063851c1706e2ae49a5b2bc83253296e8a32d96b66e0b1f606ed31eebe->enter($__internal_8a1697063851c1706e2ae49a5b2bc83253296e8a32d96b66e0b1f606ed31eebe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "core/header.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
     <head>    
        <meta charset=\"UTF-8\" />
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\" />
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />       
        
        <title>
            ";
        // line 9
        $this->displayBlock('title', $context, $blocks);
        // line 10
        echo "        </title>
        
        ";
        // line 12
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 54
        echo "  
    </head>

";
        
        $__internal_bb6d79e85e7bb246de61da58869e2c23d8715922087626a375621fa67f27dd4a->leave($__internal_bb6d79e85e7bb246de61da58869e2c23d8715922087626a375621fa67f27dd4a_prof);

        
        $__internal_8a1697063851c1706e2ae49a5b2bc83253296e8a32d96b66e0b1f606ed31eebe->leave($__internal_8a1697063851c1706e2ae49a5b2bc83253296e8a32d96b66e0b1f606ed31eebe_prof);

    }

    // line 9
    public function block_title($context, array $blocks = array())
    {
        $__internal_d22f0ef9be1f09078ecf0e8b86416e8f90729c32c0138909d8498e5f1ddb2725 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d22f0ef9be1f09078ecf0e8b86416e8f90729c32c0138909d8498e5f1ddb2725->enter($__internal_d22f0ef9be1f09078ecf0e8b86416e8f90729c32c0138909d8498e5f1ddb2725_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_f10be769d85a199a8f749fed45971a789ad70b8c75146dbc779fff41b70aa042 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f10be769d85a199a8f749fed45971a789ad70b8c75146dbc779fff41b70aa042->enter($__internal_f10be769d85a199a8f749fed45971a789ad70b8c75146dbc779fff41b70aa042_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "FoxHound";
        
        $__internal_f10be769d85a199a8f749fed45971a789ad70b8c75146dbc779fff41b70aa042->leave($__internal_f10be769d85a199a8f749fed45971a789ad70b8c75146dbc779fff41b70aa042_prof);

        
        $__internal_d22f0ef9be1f09078ecf0e8b86416e8f90729c32c0138909d8498e5f1ddb2725->leave($__internal_d22f0ef9be1f09078ecf0e8b86416e8f90729c32c0138909d8498e5f1ddb2725_prof);

    }

    // line 12
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_f8d464367b247b319c6e3a8ca39e7d87a0551922df980af4bbe397f79ffe419f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f8d464367b247b319c6e3a8ca39e7d87a0551922df980af4bbe397f79ffe419f->enter($__internal_f8d464367b247b319c6e3a8ca39e7d87a0551922df980af4bbe397f79ffe419f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_1bc1acc8b059a56887e13953244e5d35ef5f8f017c227188fadbbef9e21ba7a6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1bc1acc8b059a56887e13953244e5d35ef5f8f017c227188fadbbef9e21ba7a6->enter($__internal_1bc1acc8b059a56887e13953244e5d35ef5f8f017c227188fadbbef9e21ba7a6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 13
        echo "            <meta charset=\"utf-8\" />
            <link rel=\"stylesheet\" rel=\"apple-touch-icon\" sizes=\"76x76\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/now-ui-kit-v1.1.0/assets/img/apple-icon.png"), "html", null, true);
        echo "\" />
            <link rel=\"stylesheet\" rel=\"icon\" type=\"image/png\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/now-ui-kit-v1.1.0/assets/img/favicon.png"), "html", null, true);
        echo "\" />
            <link rel=\"stylesheet\" href=\"https://fonts.googleapis.com/css?family=Montserrat:400,700,200\" />
            <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css\" />
            <link rel=\"stylesheet\" href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/now-ui-kit-v1.1.0/assets/css/bootstrap.min.css"), "html", null, true);
        echo "\" />
            <link rel=\"stylesheet\" href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/now-ui-kit-v1.1.0/assets/css/now-ui-kit.css"), "html", null, true);
        echo "\" />
            <link rel=\"stylesheet\" href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/now-ui-kit-v1.1.0/assets/css/demo.css"), "html", null, true);
        echo "\" />
            <style>
                .txarea {
                    width: 100%;
                    font-size: 15px;
                    padding-left: 10px;
                    padding-top: 10px;
                    padding-right: 10px;
                    padding-bottom: 0px;
                    border-bottom-width: 2px;
                    border-style: solid;
                }
                .distxarea {
                    width: 100%;
                    font-size: 15px;
                    padding-left: 10px;
                    padding-top: 10px;
                    padding-right: 10px;
                    padding-bottom: 0px;
                    border-bottom-width: 2px;
                    border-style: solid;
                }
                .txbox {
                    width: 100%;
                    font-size: 15px;
                    padding-left: 10px;
                    padding-top: 10px;
                    padding-right: 10px;
                    padding-bottom: 13px;
                    border-bottom-width: 1px;
                    border-bottom-top: 2px;
                    border-style: solid;
                }
            </style>
        ";
        
        $__internal_1bc1acc8b059a56887e13953244e5d35ef5f8f017c227188fadbbef9e21ba7a6->leave($__internal_1bc1acc8b059a56887e13953244e5d35ef5f8f017c227188fadbbef9e21ba7a6_prof);

        
        $__internal_f8d464367b247b319c6e3a8ca39e7d87a0551922df980af4bbe397f79ffe419f->leave($__internal_f8d464367b247b319c6e3a8ca39e7d87a0551922df980af4bbe397f79ffe419f_prof);

    }

    public function getTemplateName()
    {
        return "core/header.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  106 => 20,  102 => 19,  98 => 18,  92 => 15,  88 => 14,  85 => 13,  76 => 12,  58 => 9,  45 => 54,  43 => 12,  39 => 10,  37 => 9,  27 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html lang=\"en\">
     <head>    
        <meta charset=\"UTF-8\" />
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\" />
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />       
        
        <title>
            {% block title %}FoxHound{% endblock %}
        </title>
        
        {% block stylesheets %}
            <meta charset=\"utf-8\" />
            <link rel=\"stylesheet\" rel=\"apple-touch-icon\" sizes=\"76x76\" href=\"{{asset('assets/now-ui-kit-v1.1.0/assets/img/apple-icon.png')}}\" />
            <link rel=\"stylesheet\" rel=\"icon\" type=\"image/png\" href=\"{{asset('assets/now-ui-kit-v1.1.0/assets/img/favicon.png')}}\" />
            <link rel=\"stylesheet\" href=\"https://fonts.googleapis.com/css?family=Montserrat:400,700,200\" />
            <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css\" />
            <link rel=\"stylesheet\" href=\"{{asset('assets/now-ui-kit-v1.1.0/assets/css/bootstrap.min.css')}}\" />
            <link rel=\"stylesheet\" href=\"{{asset('assets/now-ui-kit-v1.1.0/assets/css/now-ui-kit.css')}}\" />
            <link rel=\"stylesheet\" href=\"{{asset('assets/now-ui-kit-v1.1.0/assets/css/demo.css')}}\" />
            <style>
                .txarea {
                    width: 100%;
                    font-size: 15px;
                    padding-left: 10px;
                    padding-top: 10px;
                    padding-right: 10px;
                    padding-bottom: 0px;
                    border-bottom-width: 2px;
                    border-style: solid;
                }
                .distxarea {
                    width: 100%;
                    font-size: 15px;
                    padding-left: 10px;
                    padding-top: 10px;
                    padding-right: 10px;
                    padding-bottom: 0px;
                    border-bottom-width: 2px;
                    border-style: solid;
                }
                .txbox {
                    width: 100%;
                    font-size: 15px;
                    padding-left: 10px;
                    padding-top: 10px;
                    padding-right: 10px;
                    padding-bottom: 13px;
                    border-bottom-width: 1px;
                    border-bottom-top: 2px;
                    border-style: solid;
                }
            </style>
        {% endblock %}  
    </head>

", "core/header.html.twig", "/home/babypandalabs/microblog/app/Resources/views/core/header.html.twig");
    }
}
