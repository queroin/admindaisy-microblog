<?php

/* @FOSUser/layout.html.twig */
class __TwigTemplate_67ba3c72f6997b99038689b5b63531c749c36d34fd1e169d8f32364de90151d4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_50153e685faf38796185f02004c348eb03389d7b49446419ed24a21a79e3f62d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_50153e685faf38796185f02004c348eb03389d7b49446419ed24a21a79e3f62d->enter($__internal_50153e685faf38796185f02004c348eb03389d7b49446419ed24a21a79e3f62d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/layout.html.twig"));

        $__internal_e93804ce62c23334388919db34937abe7a8e7927aaf2ed607bbdb3789a7ae671 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e93804ce62c23334388919db34937abe7a8e7927aaf2ed607bbdb3789a7ae671->enter($__internal_e93804ce62c23334388919db34937abe7a8e7927aaf2ed607bbdb3789a7ae671_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/layout.html.twig"));

        // line 1
        echo "
";
        // line 2
        $this->loadTemplate("core/header.html.twig", "@FOSUser/layout.html.twig", 2)->display($context);
        echo "    

    <body class=\"login-page\">
        <div>
            ";
        // line 14
        echo "        </div>

        ";
        // line 16
        if ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "hasPreviousSession", array())) {
            // line 17
            echo "            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashBag", array()), "all", array()));
            foreach ($context['_seq'] as $context["type"] => $context["messages"]) {
                // line 18
                echo "                ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($context["messages"]);
                foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                    // line 19
                    echo "                    <div class=\"";
                    echo twig_escape_filter($this->env, $context["type"], "html", null, true);
                    echo "\">
                        ";
                    // line 20
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($context["message"], array(), "FOSUserBundle"), "html", null, true);
                    echo "
                    </div>
                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 23
                echo "            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['type'], $context['messages'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 24
            echo "        ";
        }
        // line 25
        echo "
        <div>
            ";
        // line 27
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 29
        echo "        </div>

";
        // line 31
        $this->loadTemplate("core/footer.html.twig", "@FOSUser/layout.html.twig", 31)->display($context);
        echo "   


";
        
        $__internal_50153e685faf38796185f02004c348eb03389d7b49446419ed24a21a79e3f62d->leave($__internal_50153e685faf38796185f02004c348eb03389d7b49446419ed24a21a79e3f62d_prof);

        
        $__internal_e93804ce62c23334388919db34937abe7a8e7927aaf2ed607bbdb3789a7ae671->leave($__internal_e93804ce62c23334388919db34937abe7a8e7927aaf2ed607bbdb3789a7ae671_prof);

    }

    // line 27
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_37958c3749b1128c8277a6d7061759b15a91e8ff621aff82e716a264c8d5c417 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_37958c3749b1128c8277a6d7061759b15a91e8ff621aff82e716a264c8d5c417->enter($__internal_37958c3749b1128c8277a6d7061759b15a91e8ff621aff82e716a264c8d5c417_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_b6790f4bc2539a5004c7b73968f8c03f94a4dbb0f3a3218594b01c34f77e29b2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b6790f4bc2539a5004c7b73968f8c03f94a4dbb0f3a3218594b01c34f77e29b2->enter($__internal_b6790f4bc2539a5004c7b73968f8c03f94a4dbb0f3a3218594b01c34f77e29b2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 28
        echo "            ";
        
        $__internal_b6790f4bc2539a5004c7b73968f8c03f94a4dbb0f3a3218594b01c34f77e29b2->leave($__internal_b6790f4bc2539a5004c7b73968f8c03f94a4dbb0f3a3218594b01c34f77e29b2_prof);

        
        $__internal_37958c3749b1128c8277a6d7061759b15a91e8ff621aff82e716a264c8d5c417->leave($__internal_37958c3749b1128c8277a6d7061759b15a91e8ff621aff82e716a264c8d5c417_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  108 => 28,  99 => 27,  85 => 31,  81 => 29,  79 => 27,  75 => 25,  72 => 24,  66 => 23,  57 => 20,  52 => 19,  47 => 18,  42 => 17,  40 => 16,  36 => 14,  29 => 2,  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("
{% include 'core/header.html.twig' %}    

    <body class=\"login-page\">
        <div>
            {# {% if is_granted(\"IS_AUTHENTICATED_REMEMBERED\") %}
                {{ 'layout.logged_in_as'|trans({'%username%': app.user.username}, 'FOSUserBundle') }} |
                <a href=\"{{ path('fos_user_security_logout') }}\">
                    {{ 'layout.logout'|trans({}, 'FOSUserBundle') }}
                </a>
            {% else %}
                <a href=\"{{ path('fos_user_security_login') }}\">{{ 'layout.login'|trans({}, 'FOSUserBundle') }}</a>
            {% endif %} #}
        </div>

        {% if app.request.hasPreviousSession %}
            {% for type, messages in app.session.flashBag.all %}
                {% for message in messages %}
                    <div class=\"{{ type }}\">
                        {{ message|trans({}, 'FOSUserBundle') }}
                    </div>
                {% endfor %}
            {% endfor %}
        {% endif %}

        <div>
            {% block fos_user_content %}
            {% endblock fos_user_content %}
        </div>

{% include 'core/footer.html.twig' %}   


", "@FOSUser/layout.html.twig", "/home/babypandalabs/microblog/app/Resources/FOSUserBundle/views/layout.html.twig");
    }
}
