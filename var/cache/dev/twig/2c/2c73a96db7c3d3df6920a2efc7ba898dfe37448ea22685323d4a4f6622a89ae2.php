<?php

/* @WebProfiler/Collector/exception.html.twig */
class __TwigTemplate_9686a4200268e8228f08a645977c6ab94f42aa617b6e80356a4d3defaa116264 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1673f7b4f06d5628e89cf7bc5f7b14a16014727c53fc3049eb428b53a83b864d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1673f7b4f06d5628e89cf7bc5f7b14a16014727c53fc3049eb428b53a83b864d->enter($__internal_1673f7b4f06d5628e89cf7bc5f7b14a16014727c53fc3049eb428b53a83b864d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $__internal_aaff73eed10a4c481ce074e8b69f190f138f99bd0cc3b16bce53f2ba677ea8a7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_aaff73eed10a4c481ce074e8b69f190f138f99bd0cc3b16bce53f2ba677ea8a7->enter($__internal_aaff73eed10a4c481ce074e8b69f190f138f99bd0cc3b16bce53f2ba677ea8a7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_1673f7b4f06d5628e89cf7bc5f7b14a16014727c53fc3049eb428b53a83b864d->leave($__internal_1673f7b4f06d5628e89cf7bc5f7b14a16014727c53fc3049eb428b53a83b864d_prof);

        
        $__internal_aaff73eed10a4c481ce074e8b69f190f138f99bd0cc3b16bce53f2ba677ea8a7->leave($__internal_aaff73eed10a4c481ce074e8b69f190f138f99bd0cc3b16bce53f2ba677ea8a7_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_af9a0fca5b249d8ee091a3e11d4f3f80a463c21ba3f14b1a72774879b3311abb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_af9a0fca5b249d8ee091a3e11d4f3f80a463c21ba3f14b1a72774879b3311abb->enter($__internal_af9a0fca5b249d8ee091a3e11d4f3f80a463c21ba3f14b1a72774879b3311abb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_facf924b70f67e4f1ca73566c1c75a0c9483d56718db0caf1c0447442fca048e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_facf924b70f67e4f1ca73566c1c75a0c9483d56718db0caf1c0447442fca048e->enter($__internal_facf924b70f67e4f1ca73566c1c75a0c9483d56718db0caf1c0447442fca048e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if ($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_facf924b70f67e4f1ca73566c1c75a0c9483d56718db0caf1c0447442fca048e->leave($__internal_facf924b70f67e4f1ca73566c1c75a0c9483d56718db0caf1c0447442fca048e_prof);

        
        $__internal_af9a0fca5b249d8ee091a3e11d4f3f80a463c21ba3f14b1a72774879b3311abb->leave($__internal_af9a0fca5b249d8ee091a3e11d4f3f80a463c21ba3f14b1a72774879b3311abb_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_bac0392fa468ec2cbe7cd0a88bde993ae8664edba802b2dbc10a54e22b76665a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bac0392fa468ec2cbe7cd0a88bde993ae8664edba802b2dbc10a54e22b76665a->enter($__internal_bac0392fa468ec2cbe7cd0a88bde993ae8664edba802b2dbc10a54e22b76665a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_e0521d7bf3b38878210e339dc3bc0b446be991cafa79c0eafc63c3861dc3b3ba = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e0521d7bf3b38878210e339dc3bc0b446be991cafa79c0eafc63c3861dc3b3ba->enter($__internal_e0521d7bf3b38878210e339dc3bc0b446be991cafa79c0eafc63c3861dc3b3ba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo (($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if ($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_e0521d7bf3b38878210e339dc3bc0b446be991cafa79c0eafc63c3861dc3b3ba->leave($__internal_e0521d7bf3b38878210e339dc3bc0b446be991cafa79c0eafc63c3861dc3b3ba_prof);

        
        $__internal_bac0392fa468ec2cbe7cd0a88bde993ae8664edba802b2dbc10a54e22b76665a->leave($__internal_bac0392fa468ec2cbe7cd0a88bde993ae8664edba802b2dbc10a54e22b76665a_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_e257116c63438e20f763623fa6db279e1fd461917a87a7c96c89cef384d49c50 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e257116c63438e20f763623fa6db279e1fd461917a87a7c96c89cef384d49c50->enter($__internal_e257116c63438e20f763623fa6db279e1fd461917a87a7c96c89cef384d49c50_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_17500ab6a1fac4530722177530a26c187d62c4d6ddc813de7e1c1097c25fdaf4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_17500ab6a1fac4530722177530a26c187d62c4d6ddc813de7e1c1097c25fdaf4->enter($__internal_17500ab6a1fac4530722177530a26c187d62c4d6ddc813de7e1c1097c25fdaf4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !$this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
            echo "
        </div>
    ";
        }
        
        $__internal_17500ab6a1fac4530722177530a26c187d62c4d6ddc813de7e1c1097c25fdaf4->leave($__internal_17500ab6a1fac4530722177530a26c187d62c4d6ddc813de7e1c1097c25fdaf4_prof);

        
        $__internal_e257116c63438e20f763623fa6db279e1fd461917a87a7c96c89cef384d49c50->leave($__internal_e257116c63438e20f763623fa6db279e1fd461917a87a7c96c89cef384d49c50_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  135 => 32,  129 => 28,  127 => 27,  123 => 25,  114 => 24,  103 => 21,  97 => 17,  95 => 16,  90 => 14,  85 => 13,  76 => 12,  63 => 9,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "@WebProfiler/Collector/exception.html.twig", "/home/babypandalabs/microblog/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/exception.html.twig");
    }
}
