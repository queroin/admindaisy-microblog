<?php

/* @FOSUser/Registration/register.html.twig */
class __TwigTemplate_d5f13b17d3bf94261c5e9b86745780376018cef8208aa10cce0800f586f30433 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Registration/register.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_348b7dc68a3923cb905638f60af3fa5553b195e31508f3b91a256219f26edf65 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_348b7dc68a3923cb905638f60af3fa5553b195e31508f3b91a256219f26edf65->enter($__internal_348b7dc68a3923cb905638f60af3fa5553b195e31508f3b91a256219f26edf65_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/register.html.twig"));

        $__internal_4018afd2aacde04c2b3d8454bc654bd0b3f2c182b0e6b0b491f554e2263fc43f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4018afd2aacde04c2b3d8454bc654bd0b3f2c182b0e6b0b491f554e2263fc43f->enter($__internal_4018afd2aacde04c2b3d8454bc654bd0b3f2c182b0e6b0b491f554e2263fc43f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/register.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_348b7dc68a3923cb905638f60af3fa5553b195e31508f3b91a256219f26edf65->leave($__internal_348b7dc68a3923cb905638f60af3fa5553b195e31508f3b91a256219f26edf65_prof);

        
        $__internal_4018afd2aacde04c2b3d8454bc654bd0b3f2c182b0e6b0b491f554e2263fc43f->leave($__internal_4018afd2aacde04c2b3d8454bc654bd0b3f2c182b0e6b0b491f554e2263fc43f_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_d596d332e53a05c1bd67ab89553e3fbc6b48818f11515207f95cfef2819fb569 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d596d332e53a05c1bd67ab89553e3fbc6b48818f11515207f95cfef2819fb569->enter($__internal_d596d332e53a05c1bd67ab89553e3fbc6b48818f11515207f95cfef2819fb569_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_5296cb14be44497b315f4a9d4e16fff3a11f413d5009e34277358b943ad37d3d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5296cb14be44497b315f4a9d4e16fff3a11f413d5009e34277358b943ad37d3d->enter($__internal_5296cb14be44497b315f4a9d4e16fff3a11f413d5009e34277358b943ad37d3d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Registration/register_content.html.twig", "@FOSUser/Registration/register.html.twig", 4)->display($context);
        
        $__internal_5296cb14be44497b315f4a9d4e16fff3a11f413d5009e34277358b943ad37d3d->leave($__internal_5296cb14be44497b315f4a9d4e16fff3a11f413d5009e34277358b943ad37d3d_prof);

        
        $__internal_d596d332e53a05c1bd67ab89553e3fbc6b48818f11515207f95cfef2819fb569->leave($__internal_d596d332e53a05c1bd67ab89553e3fbc6b48818f11515207f95cfef2819fb569_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Registration/register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Registration/register_content.html.twig\" %}
{% endblock fos_user_content %}
", "@FOSUser/Registration/register.html.twig", "/home/babypandalabs/microblog/app/Resources/FOSUserBundle/views/Registration/register.html.twig");
    }
}
