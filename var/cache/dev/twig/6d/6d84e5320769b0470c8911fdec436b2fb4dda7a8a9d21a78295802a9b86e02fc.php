<?php

/* post/index.html.twig */
class __TwigTemplate_ab36a38e20d34d43e2d4fee8bd7703cef35c0d47a6bb5006a81eefc05796b3fd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "post/index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c56b747d97509980eb0d073e6c7f090d1e81ba23926e8463cc8485eeab285c43 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c56b747d97509980eb0d073e6c7f090d1e81ba23926e8463cc8485eeab285c43->enter($__internal_c56b747d97509980eb0d073e6c7f090d1e81ba23926e8463cc8485eeab285c43_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "post/index.html.twig"));

        $__internal_28911f37ca303858b0a11e3d5fd043616070b531382dc092984c418addec4002 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_28911f37ca303858b0a11e3d5fd043616070b531382dc092984c418addec4002->enter($__internal_28911f37ca303858b0a11e3d5fd043616070b531382dc092984c418addec4002_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "post/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c56b747d97509980eb0d073e6c7f090d1e81ba23926e8463cc8485eeab285c43->leave($__internal_c56b747d97509980eb0d073e6c7f090d1e81ba23926e8463cc8485eeab285c43_prof);

        
        $__internal_28911f37ca303858b0a11e3d5fd043616070b531382dc092984c418addec4002->leave($__internal_28911f37ca303858b0a11e3d5fd043616070b531382dc092984c418addec4002_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_5b0c1f28a05ecc1bfcc997c46dc91708bd514fc53601b7548ade5efc3194147d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5b0c1f28a05ecc1bfcc997c46dc91708bd514fc53601b7548ade5efc3194147d->enter($__internal_5b0c1f28a05ecc1bfcc997c46dc91708bd514fc53601b7548ade5efc3194147d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_ff15b3edd0b22b96f4507b897071ddcbf59891162890e0b08046406706b03654 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ff15b3edd0b22b96f4507b897071ddcbf59891162890e0b08046406706b03654->enter($__internal_ff15b3edd0b22b96f4507b897071ddcbf59891162890e0b08046406706b03654_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Posts list</h1>

    <table>
        <thead>
            <tr>
                <th>Id</th>
                <th>Username</th>
                <th>Title</th>
                <th>Topic</th>
                <th>Blog_mess</th>
                <th>Blog_img</th>
                <th>Posted_at</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        ";
        // line 20
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["posts"]) ? $context["posts"] : $this->getContext($context, "posts")));
        foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
            // line 21
            echo "            <tr>
                <td><a href=\"";
            // line 22
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("post_show", array("id" => $this->getAttribute($context["post"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "id", array()), "html", null, true);
            echo "</a></td>
                <td>";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "username", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "title", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 25
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "topic", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 26
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "blogmess", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 27
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "blogimg", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 28
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "postedat", array()), "html", null, true);
            echo "</td>
                <td>
                    <ul>
                        <li>
                            <a href=\"";
            // line 32
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("post_show", array("id" => $this->getAttribute($context["post"], "id", array()))), "html", null, true);
            echo "\">show</a>
                        </li>
                        <li>
                            <a href=\"";
            // line 35
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("post_edit", array("id" => $this->getAttribute($context["post"], "id", array()))), "html", null, true);
            echo "\">edit</a>
                        </li>
                    </ul>
                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 41
        echo "        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"";
        // line 46
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("post_new");
        echo "\">Create a new post</a>
        </li>
    </ul>
";
        
        $__internal_ff15b3edd0b22b96f4507b897071ddcbf59891162890e0b08046406706b03654->leave($__internal_ff15b3edd0b22b96f4507b897071ddcbf59891162890e0b08046406706b03654_prof);

        
        $__internal_5b0c1f28a05ecc1bfcc997c46dc91708bd514fc53601b7548ade5efc3194147d->leave($__internal_5b0c1f28a05ecc1bfcc997c46dc91708bd514fc53601b7548ade5efc3194147d_prof);

    }

    public function getTemplateName()
    {
        return "post/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  132 => 46,  125 => 41,  113 => 35,  107 => 32,  100 => 28,  96 => 27,  92 => 26,  88 => 25,  84 => 24,  80 => 23,  74 => 22,  71 => 21,  67 => 20,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Posts list</h1>

    <table>
        <thead>
            <tr>
                <th>Id</th>
                <th>Username</th>
                <th>Title</th>
                <th>Topic</th>
                <th>Blog_mess</th>
                <th>Blog_img</th>
                <th>Posted_at</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        {% for post in posts %}
            <tr>
                <td><a href=\"{{ path('post_show', { 'id': post.id }) }}\">{{ post.id }}</a></td>
                <td>{{ post.username }}</td>
                <td>{{ post.title }}</td>
                <td>{{ post.topic }}</td>
                <td>{{ post.blogmess }}</td>
                <td>{{ post.blogimg }}</td>
                <td>{{ post.postedat }}</td>
                <td>
                    <ul>
                        <li>
                            <a href=\"{{ path('post_show', { 'id': post.id }) }}\">show</a>
                        </li>
                        <li>
                            <a href=\"{{ path('post_edit', { 'id': post.id }) }}\">edit</a>
                        </li>
                    </ul>
                </td>
            </tr>
        {% endfor %}
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"{{ path('post_new') }}\">Create a new post</a>
        </li>
    </ul>
{% endblock %}
", "post/index.html.twig", "/home/babypandalabs/microblog/app/Resources/views/post/index.html.twig");
    }
}
