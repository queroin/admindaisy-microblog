<?php

/* body/body_header.html.twig */
class __TwigTemplate_2b8fb1a97a46b23c1470a99a8db6206b3f1786b70434073e05d76787df4b1a82 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_51587ec6e358013698b807f5afb9210235d141ad269faad70821071e2ffec659 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_51587ec6e358013698b807f5afb9210235d141ad269faad70821071e2ffec659->enter($__internal_51587ec6e358013698b807f5afb9210235d141ad269faad70821071e2ffec659_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "body/body_header.html.twig"));

        $__internal_795cb1c3567f84aedf4b4c01ac77461a16cdb9d35b2c8a7c4ea97e61a884fc66 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_795cb1c3567f84aedf4b4c01ac77461a16cdb9d35b2c8a7c4ea97e61a884fc66->enter($__internal_795cb1c3567f84aedf4b4c01ac77461a16cdb9d35b2c8a7c4ea97e61a884fc66_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "body/body_header.html.twig"));

        // line 2
        echo "<body class=\"index-page sidebar-collapse\">
    <nav class=\"navbar navbar-expand-lg bg-primary fixed-top\" color-on-scroll=\"400\">
        <div class=\"container\">
            <div class=\"collapse navbar-collapse justify-content-end\" id=\"navigation\" data-nav-image=\"./assets/img/blurred-image-1.jpg\">
                <ul class=\"navbar-nav\">
                    <li class=\"nav-item\">
                        <a class=\"nav-link\" href=\"/post/new\">
                            <i class=\"fa fa-heart\"></i>
                            ";
        // line 10
        if ($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array())) {
            // line 11
            echo "                            <p><strong style=\"font-size: 13px;\">Hi, ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "html", null, true);
            echo "</strong></p>
                            ";
        }
        // line 13
        echo "                        </a>
                    </li>
                    <li class=\"active nav-item\">
                        <a class=\"nav-link\" href=\"/home\">
                            <i class=\"fa fa-home\"></i>
                            <p p class=\"d-lg-none d-xl-none\">Home</p>
                        </a>
                    </li>
                    <li class=\"nav-item\">
                        <a class=\"nav-link\" href=\"";
        // line 22
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_profile_edit");
        echo "\">
                            <i class=\"fa fa-gear\"></i>
                            <p class=\"d-lg-none d-xl-none\">Settings</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>";
        
        $__internal_51587ec6e358013698b807f5afb9210235d141ad269faad70821071e2ffec659->leave($__internal_51587ec6e358013698b807f5afb9210235d141ad269faad70821071e2ffec659_prof);

        
        $__internal_795cb1c3567f84aedf4b4c01ac77461a16cdb9d35b2c8a7c4ea97e61a884fc66->leave($__internal_795cb1c3567f84aedf4b4c01ac77461a16cdb9d35b2c8a7c4ea97e61a884fc66_prof);

    }

    public function getTemplateName()
    {
        return "body/body_header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  54 => 22,  43 => 13,  37 => 11,  35 => 10,  25 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# Header #}
<body class=\"index-page sidebar-collapse\">
    <nav class=\"navbar navbar-expand-lg bg-primary fixed-top\" color-on-scroll=\"400\">
        <div class=\"container\">
            <div class=\"collapse navbar-collapse justify-content-end\" id=\"navigation\" data-nav-image=\"./assets/img/blurred-image-1.jpg\">
                <ul class=\"navbar-nav\">
                    <li class=\"nav-item\">
                        <a class=\"nav-link\" href=\"/post/new\">
                            <i class=\"fa fa-heart\"></i>
                            {% if app.user %}
                            <p><strong style=\"font-size: 13px;\">Hi, {{user.username}}</strong></p>
                            {% endif %}
                        </a>
                    </li>
                    <li class=\"active nav-item\">
                        <a class=\"nav-link\" href=\"/home\">
                            <i class=\"fa fa-home\"></i>
                            <p p class=\"d-lg-none d-xl-none\">Home</p>
                        </a>
                    </li>
                    <li class=\"nav-item\">
                        <a class=\"nav-link\" href=\"{{path('fos_user_profile_edit')}}\">
                            <i class=\"fa fa-gear\"></i>
                            <p class=\"d-lg-none d-xl-none\">Settings</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>", "body/body_header.html.twig", "/home/babypandalabs/microblog/app/Resources/views/body/body_header.html.twig");
    }
}
