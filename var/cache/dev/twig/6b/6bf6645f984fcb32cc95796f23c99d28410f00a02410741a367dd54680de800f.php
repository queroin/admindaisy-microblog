<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_0daaa3d5cfd073d06a3840dd526eb53dc2929fac357308915d14d50f05ae0819 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1aa27b5a57d0f760db4b2428ddb58704b108fcb829bfc75919f5fce94a9a749e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1aa27b5a57d0f760db4b2428ddb58704b108fcb829bfc75919f5fce94a9a749e->enter($__internal_1aa27b5a57d0f760db4b2428ddb58704b108fcb829bfc75919f5fce94a9a749e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $__internal_6cddbe0d1e3e748cad15a507c5711cb55f1ccdd2d0357789217f175db56014bf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6cddbe0d1e3e748cad15a507c5711cb55f1ccdd2d0357789217f175db56014bf->enter($__internal_6cddbe0d1e3e748cad15a507c5711cb55f1ccdd2d0357789217f175db56014bf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_1aa27b5a57d0f760db4b2428ddb58704b108fcb829bfc75919f5fce94a9a749e->leave($__internal_1aa27b5a57d0f760db4b2428ddb58704b108fcb829bfc75919f5fce94a9a749e_prof);

        
        $__internal_6cddbe0d1e3e748cad15a507c5711cb55f1ccdd2d0357789217f175db56014bf->leave($__internal_6cddbe0d1e3e748cad15a507c5711cb55f1ccdd2d0357789217f175db56014bf_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_67e3fadbcf34803e1ae31587e688fcfeab1d0879f03468576c127332c530c7d8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_67e3fadbcf34803e1ae31587e688fcfeab1d0879f03468576c127332c530c7d8->enter($__internal_67e3fadbcf34803e1ae31587e688fcfeab1d0879f03468576c127332c530c7d8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_bdbd92908b6168a7fd7b7b844285e005a365fc99f75aef03843e7096e2213733 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bdbd92908b6168a7fd7b7b844285e005a365fc99f75aef03843e7096e2213733->enter($__internal_bdbd92908b6168a7fd7b7b844285e005a365fc99f75aef03843e7096e2213733_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_bdbd92908b6168a7fd7b7b844285e005a365fc99f75aef03843e7096e2213733->leave($__internal_bdbd92908b6168a7fd7b7b844285e005a365fc99f75aef03843e7096e2213733_prof);

        
        $__internal_67e3fadbcf34803e1ae31587e688fcfeab1d0879f03468576c127332c530c7d8->leave($__internal_67e3fadbcf34803e1ae31587e688fcfeab1d0879f03468576c127332c530c7d8_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_85fd2194fa20ed5d6a625a10909234bf3745679828e450d17b7dedf2dac58717 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_85fd2194fa20ed5d6a625a10909234bf3745679828e450d17b7dedf2dac58717->enter($__internal_85fd2194fa20ed5d6a625a10909234bf3745679828e450d17b7dedf2dac58717_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_3b68c2576a3a443f0c9c71647aecdece856efcb18c9241e0e115ad1341ff9e06 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3b68c2576a3a443f0c9c71647aecdece856efcb18c9241e0e115ad1341ff9e06->enter($__internal_3b68c2576a3a443f0c9c71647aecdece856efcb18c9241e0e115ad1341ff9e06_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_3b68c2576a3a443f0c9c71647aecdece856efcb18c9241e0e115ad1341ff9e06->leave($__internal_3b68c2576a3a443f0c9c71647aecdece856efcb18c9241e0e115ad1341ff9e06_prof);

        
        $__internal_85fd2194fa20ed5d6a625a10909234bf3745679828e450d17b7dedf2dac58717->leave($__internal_85fd2194fa20ed5d6a625a10909234bf3745679828e450d17b7dedf2dac58717_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_4781edb1c1867402581c467272d4858f6db158742df51ce2c62979069dae1dd8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4781edb1c1867402581c467272d4858f6db158742df51ce2c62979069dae1dd8->enter($__internal_4781edb1c1867402581c467272d4858f6db158742df51ce2c62979069dae1dd8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_58c6c09d306d5f5349cdfa0e15640d79bbe1e038101c006f31a3966ef33df655 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_58c6c09d306d5f5349cdfa0e15640d79bbe1e038101c006f31a3966ef33df655->enter($__internal_58c6c09d306d5f5349cdfa0e15640d79bbe1e038101c006f31a3966ef33df655_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_58c6c09d306d5f5349cdfa0e15640d79bbe1e038101c006f31a3966ef33df655->leave($__internal_58c6c09d306d5f5349cdfa0e15640d79bbe1e038101c006f31a3966ef33df655_prof);

        
        $__internal_4781edb1c1867402581c467272d4858f6db158742df51ce2c62979069dae1dd8->leave($__internal_4781edb1c1867402581c467272d4858f6db158742df51ce2c62979069dae1dd8_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "/home/babypandalabs/microblog/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/router.html.twig");
    }
}
