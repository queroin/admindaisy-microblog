<?php

/* @WebProfiler/Collector/ajax.html.twig */
class __TwigTemplate_d8de0f99e344099204aff626deb5cbc0b82f7106ce08a3ffc6e5461e5316ca23 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/ajax.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_17ee89e3aeed81eba0f0ca6b3ce4e94fca55133547528a8f836652fed5e870f3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_17ee89e3aeed81eba0f0ca6b3ce4e94fca55133547528a8f836652fed5e870f3->enter($__internal_17ee89e3aeed81eba0f0ca6b3ce4e94fca55133547528a8f836652fed5e870f3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/ajax.html.twig"));

        $__internal_259d5d698428d20f973e56c5996f34972a0b362d1341bfa42310735d9fc8f61c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_259d5d698428d20f973e56c5996f34972a0b362d1341bfa42310735d9fc8f61c->enter($__internal_259d5d698428d20f973e56c5996f34972a0b362d1341bfa42310735d9fc8f61c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/ajax.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_17ee89e3aeed81eba0f0ca6b3ce4e94fca55133547528a8f836652fed5e870f3->leave($__internal_17ee89e3aeed81eba0f0ca6b3ce4e94fca55133547528a8f836652fed5e870f3_prof);

        
        $__internal_259d5d698428d20f973e56c5996f34972a0b362d1341bfa42310735d9fc8f61c->leave($__internal_259d5d698428d20f973e56c5996f34972a0b362d1341bfa42310735d9fc8f61c_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_dcebc5fb3d01eec6e23b310e5cddce95ee736fa95bb0f647c13b331dcd7efee4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dcebc5fb3d01eec6e23b310e5cddce95ee736fa95bb0f647c13b331dcd7efee4->enter($__internal_dcebc5fb3d01eec6e23b310e5cddce95ee736fa95bb0f647c13b331dcd7efee4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_1441fe1417ae3fc3e3ca7b310aab614dbe1409e1bc2990abe273791f99690dbd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1441fe1417ae3fc3e3ca7b310aab614dbe1409e1bc2990abe273791f99690dbd->enter($__internal_1441fe1417ae3fc3e3ca7b310aab614dbe1409e1bc2990abe273791f99690dbd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        // line 4
        echo "    ";
        ob_start();
        // line 5
        echo "        ";
        echo twig_include($this->env, $context, "@WebProfiler/Icon/ajax.svg");
        echo "
        <span class=\"sf-toolbar-value sf-toolbar-ajax-request-counter\">0</span>
    ";
        $context["icon"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 8
        echo "
    ";
        // line 9
        $context["text"] = ('' === $tmp = "        <div class=\"sf-toolbar-info-piece\">
            <b class=\"sf-toolbar-ajax-info\"></b>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <table class=\"sf-toolbar-ajax-requests\">
                <thead>
                    <tr>
                        <th>Method</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>URL</th>
                        <th>Time</th>
                        <th>Profile</th>
                    </tr>
                </thead>
                <tbody class=\"sf-toolbar-ajax-request-list\"></tbody>
            </table>
        </div>
    ") ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 29
        echo "
    ";
        // line 30
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/toolbar_item.html.twig", array("link" => false));
        echo "
";
        
        $__internal_1441fe1417ae3fc3e3ca7b310aab614dbe1409e1bc2990abe273791f99690dbd->leave($__internal_1441fe1417ae3fc3e3ca7b310aab614dbe1409e1bc2990abe273791f99690dbd_prof);

        
        $__internal_dcebc5fb3d01eec6e23b310e5cddce95ee736fa95bb0f647c13b331dcd7efee4->leave($__internal_dcebc5fb3d01eec6e23b310e5cddce95ee736fa95bb0f647c13b331dcd7efee4_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/ajax.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 30,  82 => 29,  62 => 9,  59 => 8,  52 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}
    {% set icon %}
        {{ include('@WebProfiler/Icon/ajax.svg') }}
        <span class=\"sf-toolbar-value sf-toolbar-ajax-request-counter\">0</span>
    {% endset %}

    {% set text %}
        <div class=\"sf-toolbar-info-piece\">
            <b class=\"sf-toolbar-ajax-info\"></b>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <table class=\"sf-toolbar-ajax-requests\">
                <thead>
                    <tr>
                        <th>Method</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>URL</th>
                        <th>Time</th>
                        <th>Profile</th>
                    </tr>
                </thead>
                <tbody class=\"sf-toolbar-ajax-request-list\"></tbody>
            </table>
        </div>
    {% endset %}

    {{ include('@WebProfiler/Profiler/toolbar_item.html.twig', { link: false }) }}
{% endblock %}
", "@WebProfiler/Collector/ajax.html.twig", "/home/babypandalabs/microblog/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/ajax.html.twig");
    }
}
