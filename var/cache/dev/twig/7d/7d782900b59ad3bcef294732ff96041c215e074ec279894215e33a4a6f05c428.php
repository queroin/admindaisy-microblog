<?php

/* @Twig/layout.html.twig */
class __TwigTemplate_66e7e75ec9c0010641dbe585aeeac70571ef1f0833e1a7882f58c23aa7e6cdbc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_63d49360c9de377d2ff6d332b6f396a68d83e6e9535510d8768867755e9514e1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_63d49360c9de377d2ff6d332b6f396a68d83e6e9535510d8768867755e9514e1->enter($__internal_63d49360c9de377d2ff6d332b6f396a68d83e6e9535510d8768867755e9514e1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/layout.html.twig"));

        $__internal_7cbdba34436f9dae1abfd4e4c109f623e9f2683e5e1ff5cee2df9ee34c00c08a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7cbdba34436f9dae1abfd4e4c109f623e9f2683e5e1ff5cee2df9ee34c00c08a->enter($__internal_7cbdba34436f9dae1abfd4e4c109f623e9f2683e5e1ff5cee2df9ee34c00c08a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/layout.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>";
        // line 7
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 8
        echo twig_include($this->env, $context, "@Twig/images/favicon.png.base64");
        echo "\">
        <style>";
        // line 9
        echo twig_include($this->env, $context, "@Twig/exception.css.twig");
        echo "</style>
        ";
        // line 10
        $this->displayBlock('head', $context, $blocks);
        // line 11
        echo "    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">";
        // line 15
        echo twig_include($this->env, $context, "@Twig/images/symfony-logo.svg");
        echo " Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">";
        // line 19
        echo twig_include($this->env, $context, "@Twig/images/icon-book.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">";
        // line 26
        echo twig_include($this->env, $context, "@Twig/images/icon-support.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        ";
        // line 33
        $this->displayBlock('body', $context, $blocks);
        // line 34
        echo "        ";
        echo twig_include($this->env, $context, "@Twig/base_js.html.twig");
        echo "
    </body>
</html>
";
        
        $__internal_63d49360c9de377d2ff6d332b6f396a68d83e6e9535510d8768867755e9514e1->leave($__internal_63d49360c9de377d2ff6d332b6f396a68d83e6e9535510d8768867755e9514e1_prof);

        
        $__internal_7cbdba34436f9dae1abfd4e4c109f623e9f2683e5e1ff5cee2df9ee34c00c08a->leave($__internal_7cbdba34436f9dae1abfd4e4c109f623e9f2683e5e1ff5cee2df9ee34c00c08a_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_1e751ffc08ef3f89f6d986dfec91841cd99a46fdd53db1a793de98ef0f38c4da = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1e751ffc08ef3f89f6d986dfec91841cd99a46fdd53db1a793de98ef0f38c4da->enter($__internal_1e751ffc08ef3f89f6d986dfec91841cd99a46fdd53db1a793de98ef0f38c4da_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_1a9d4d2dd3a5e4d47b429bf5c87944c86bec304249ede327e1f5dc205924fbbd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1a9d4d2dd3a5e4d47b429bf5c87944c86bec304249ede327e1f5dc205924fbbd->enter($__internal_1a9d4d2dd3a5e4d47b429bf5c87944c86bec304249ede327e1f5dc205924fbbd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        
        $__internal_1a9d4d2dd3a5e4d47b429bf5c87944c86bec304249ede327e1f5dc205924fbbd->leave($__internal_1a9d4d2dd3a5e4d47b429bf5c87944c86bec304249ede327e1f5dc205924fbbd_prof);

        
        $__internal_1e751ffc08ef3f89f6d986dfec91841cd99a46fdd53db1a793de98ef0f38c4da->leave($__internal_1e751ffc08ef3f89f6d986dfec91841cd99a46fdd53db1a793de98ef0f38c4da_prof);

    }

    // line 10
    public function block_head($context, array $blocks = array())
    {
        $__internal_73ba34364a7e3bb01bde452dbbb0fc57d81761fd8bcb374fde8ec6af156e89d8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_73ba34364a7e3bb01bde452dbbb0fc57d81761fd8bcb374fde8ec6af156e89d8->enter($__internal_73ba34364a7e3bb01bde452dbbb0fc57d81761fd8bcb374fde8ec6af156e89d8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_77c2dfa4ac77b8f75f47b7bc3335b46e46a397cb10b3033efc5fd9ce010d56c9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_77c2dfa4ac77b8f75f47b7bc3335b46e46a397cb10b3033efc5fd9ce010d56c9->enter($__internal_77c2dfa4ac77b8f75f47b7bc3335b46e46a397cb10b3033efc5fd9ce010d56c9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        
        $__internal_77c2dfa4ac77b8f75f47b7bc3335b46e46a397cb10b3033efc5fd9ce010d56c9->leave($__internal_77c2dfa4ac77b8f75f47b7bc3335b46e46a397cb10b3033efc5fd9ce010d56c9_prof);

        
        $__internal_73ba34364a7e3bb01bde452dbbb0fc57d81761fd8bcb374fde8ec6af156e89d8->leave($__internal_73ba34364a7e3bb01bde452dbbb0fc57d81761fd8bcb374fde8ec6af156e89d8_prof);

    }

    // line 33
    public function block_body($context, array $blocks = array())
    {
        $__internal_765109904d52dfbce5956cfe728b84a57076c8426b79b4408f87db8833e711ed = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_765109904d52dfbce5956cfe728b84a57076c8426b79b4408f87db8833e711ed->enter($__internal_765109904d52dfbce5956cfe728b84a57076c8426b79b4408f87db8833e711ed_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_1f93049793d42229d6c392c108515df73b7bd87d9d2ec502f5f72b6fdad5c671 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1f93049793d42229d6c392c108515df73b7bd87d9d2ec502f5f72b6fdad5c671->enter($__internal_1f93049793d42229d6c392c108515df73b7bd87d9d2ec502f5f72b6fdad5c671_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_1f93049793d42229d6c392c108515df73b7bd87d9d2ec502f5f72b6fdad5c671->leave($__internal_1f93049793d42229d6c392c108515df73b7bd87d9d2ec502f5f72b6fdad5c671_prof);

        
        $__internal_765109904d52dfbce5956cfe728b84a57076c8426b79b4408f87db8833e711ed->leave($__internal_765109904d52dfbce5956cfe728b84a57076c8426b79b4408f87db8833e711ed_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 33,  120 => 10,  103 => 7,  88 => 34,  86 => 33,  76 => 26,  66 => 19,  59 => 15,  53 => 11,  51 => 10,  47 => 9,  43 => 8,  39 => 7,  33 => 4,  28 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"{{ _charset }}\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>{% block title %}{% endblock %}</title>
        <link rel=\"icon\" type=\"image/png\" href=\"{{ include('@Twig/images/favicon.png.base64') }}\">
        <style>{{ include('@Twig/exception.css.twig') }}</style>
        {% block head %}{% endblock %}
    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">{{ include('@Twig/images/symfony-logo.svg') }} Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-book.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-support.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        {% block body %}{% endblock %}
        {{ include('@Twig/base_js.html.twig') }}
    </body>
</html>
", "@Twig/layout.html.twig", "/home/babypandalabs/microblog/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/layout.html.twig");
    }
}
