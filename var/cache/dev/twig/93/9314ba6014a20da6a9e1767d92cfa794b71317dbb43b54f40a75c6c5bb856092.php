<?php

/* @FOSUser/Profile/show_content.html.twig */
class __TwigTemplate_91c3b3fa1013c42257ee4f13b3342e06b7c8f6ab23cbf6d45e0b3def622d7c1c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6e5caa1df8bdd25d6e5353c011e8bf628666a53bcbff12616491c8cdeb0f5784 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6e5caa1df8bdd25d6e5353c011e8bf628666a53bcbff12616491c8cdeb0f5784->enter($__internal_6e5caa1df8bdd25d6e5353c011e8bf628666a53bcbff12616491c8cdeb0f5784_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Profile/show_content.html.twig"));

        $__internal_bd9345fda3fb3a01036e60cd401ae54f4f1469a698ae535e752e2d89e0f6ea41 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bd9345fda3fb3a01036e60cd401ae54f4f1469a698ae535e752e2d89e0f6ea41->enter($__internal_bd9345fda3fb3a01036e60cd401ae54f4f1469a698ae535e752e2d89e0f6ea41_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Profile/show_content.html.twig"));

        // line 2
        echo "
<div class=\"fos_user_user_show\">
    <p>";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("profile.show.username", array(), "FOSUserBundle"), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "html", null, true);
        echo "</p>
    <p>";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("profile.show.email", array(), "FOSUserBundle"), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "email", array()), "html", null, true);
        echo "</p>
</div>
";
        
        $__internal_6e5caa1df8bdd25d6e5353c011e8bf628666a53bcbff12616491c8cdeb0f5784->leave($__internal_6e5caa1df8bdd25d6e5353c011e8bf628666a53bcbff12616491c8cdeb0f5784_prof);

        
        $__internal_bd9345fda3fb3a01036e60cd401ae54f4f1469a698ae535e752e2d89e0f6ea41->leave($__internal_bd9345fda3fb3a01036e60cd401ae54f4f1469a698ae535e752e2d89e0f6ea41_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Profile/show_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  35 => 5,  29 => 4,  25 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}

<div class=\"fos_user_user_show\">
    <p>{{ 'profile.show.username'|trans }}: {{ user.username }}</p>
    <p>{{ 'profile.show.email'|trans }}: {{ user.email }}</p>
</div>
", "@FOSUser/Profile/show_content.html.twig", "/home/babypandalabs/microblog/app/Resources/FOSUserBundle/views/Profile/show_content.html.twig");
    }
}
